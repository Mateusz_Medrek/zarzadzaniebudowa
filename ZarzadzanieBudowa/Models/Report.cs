﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Models
{
    public class Report
    {
        //Właściwości;
        public int Id { get; set; } //Id raportu w BD;
        public string ReportName { get; set; } //Nazwa raportu;
        public string Content { get; set; } //Treść raportu;
        public string Author { get; set; } //Autor raportu;
        public DateTime CreateDate { get; set; } //Data utworzenia raportu;

        //Konstruktory;
        public Report() { }
        public Report(int id, string name, string content, string author, DateTime createDate)
        {
            Id = id;
            ReportName = name;
            Content = content;
            Author = author;
            CreateDate = createDate;
        }
    }
}
