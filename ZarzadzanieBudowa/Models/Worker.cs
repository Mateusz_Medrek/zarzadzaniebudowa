﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Models
{
    public class Worker
    {
        //Właściwości;
        public int Id { get; set; } //Id pracownika w BD;
        public string WorkerName { get; set; } //Imię pracownika;
        public string WorkerSurname { get; set; } //Nazwisko pracownika;
        public string Pesel { get; set; } //Pesel pracownika;
        public string EmploymentDate { get; set; } //Data zatrudnienia pracownika;
        public int StatusId { get; set; } //Id statusu pracownika;
        public string Status { get; set; } //Status pracownika;

        //Konstruktory;
        public Worker() { }
        public Worker(int id, string workerName, string workerSurname, string pesel, string employmentDate, 
            int statusId, string status)
        {
            Id = id;
            WorkerName = workerName;
            WorkerSurname = workerSurname;
            Pesel = pesel;
            EmploymentDate = employmentDate;
            StatusId = statusId;
            Status = status;
        }
    }
}
