﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Models
{
    public class Materials
    {
        //Właściwości;
        public int Id { get; set; } //Id materiałów w BD;
        public string Category { get; set; } //Kategoria materiałów;
        public string Description { get; set; } //Opis materiałów;
        public string Quantity { get; set; } //Ilość materiałów;
        public string Unit { get; set; } //Jednostka materiałów (zależna od kategorii);

        //Konstruktory;
        public Materials() { }
        public Materials(int id, string category, string description, string quantity, string unit)
        {
            Id = id;
            Category = category;
            Description = description;
            Quantity = quantity;
            Unit = unit;
        }
    }
}
