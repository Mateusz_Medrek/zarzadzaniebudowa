﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Models
{
    public class Order
    {
        //Właściwości;
        public int Id { get; set; } //Id zamówienia w bazie;
        public string CreateDate { get; set; } //Data utworzenia zamówienia;
        public int StatusId { get; set; } //Id statusu zamówienia w BD;
        public string Status { get; set; } //Status zamówienia;
        public string Description { get; set; } //Opis zamówienia;

        //Konstruktory;
        public Order() { }
        public Order(int id, string createDate, int statusId, string status, string description)
        {
            Id = id;
            CreateDate = createDate;
            StatusId = statusId;
            Status = status;
            Description = description;
        }
    }
}
