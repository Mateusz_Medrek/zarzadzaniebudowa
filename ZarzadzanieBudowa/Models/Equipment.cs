﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Models
{
    public class Equipment
    {
        //Właściwości;
        public int Id { get; set; } //Id sprzętu w BD;
        public string Category { get; set; } //Kategoria sprzętu;
        public string Description { get; set; } //Opis sprzętu;
        public bool IsAvailable { get; set; } //Czy sprzęt jest dostępny do wypożyczenia;

        //Konstruktory;
        public Equipment() { }
        public Equipment(int id, string category, string description, bool isAvailable)
        {
            Id = id;
            Category = category;
            Description = description;
            IsAvailable = isAvailable;
        }
    }
}
