﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa
{
    internal class DBParameter
    {//Klasa pomocnicza, używana do przekazywania parametrów zapytań bazodanowych;
        public string Name { get; set; } //Nazwa parametru;
        public object Value { get; set; } //Wartość parametru;
        public DBParameter(string name, object value)
        {
            Name = name;
            Value = value;
        }
    }
}
