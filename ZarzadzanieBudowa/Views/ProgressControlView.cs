﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class ProgressControlView : Form
    {
        public ProgressControlView()
        {
            InitializeComponent();
        }

        private void ShowZamowienia()
        {
            #region Filling our panel with data
            //Wyszukaj wszystkie listy zadań;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM listy_zadan");
            //Wyczyść kontener;
            tlp1.Controls.Clear();
            tlp1.RowCount = 2;
            //Ustal co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdej listy zadań;
            {
                //Dodaj listęzadań do kontenera;
                tlp1.Controls.Add(new ProgressControlRowView
                {
                    Row_Id = row.Field<int>("lista_zadan_id"),
                    Nazwa = row.Field<string>("lista_zadan_nazwa"),
                    Realizacji = row.Field<DateTime>("lista_zadan_dr").ToLongDateString(),
                    Utworzenia = row.Field<DateTime>("lista_zadan_du").ToLongDateString(),
                    OstatniejModyfikacji = row.Field<DateTime>("lista_zadan_dom").ToLongDateString()
                }, 0, tlp1.RowCount - 1);
            }
            #endregion
        }

        private void MagazynZamowienia_Load(object sender, EventArgs e)
        {
            ShowZamowienia();
        }
    }
}
