﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public interface IMaterialsView
    {
        List<IMaterialsRowView> MaterialsRowViews { get; set; } //Zbiór materiałów jako elementów graficznych;
        List<string> Categories { get; set; } //Zbiór kategorii materiałów;
        void SetController(MaterialsController controller); //Ustaw kontroler dla widoku;
        void AddMaterialsToView(Materials materials); //Dodaj materiały do widoku;
        void ClearView(); //Wyczyść widok;
        void RemoveMaterialsFromView(Materials materials); //Usuń materiały z widoku;
    }
}
