﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public interface INewEditWorkersView
    {
        DataTable Categories { get; set; }
        string CategoriesDisplayMember { get; set; }
        string CategoriesValueMember { get; set; }
        object CategoriesSelectedValue { get; set; }
        int Id { get; set; }
        string WorkerName { get; set; }
        string WorkerSurname { get; set; }
        string Pesel { get; set; }
        DateTime EmploymentDate { get; set; }
        void SetController(NewEditWorkerController controller);
    }
}
