﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class BuildingTaskView : UserControl
    {
        private bool czyskonczone; //Czy zadanie zostało zakończone;

        public int Row_Id { get; set; } //Id zadania;
        public string Tresc { get; set; } //Tresc zadania;
        public string Nazwa //Nazwa zadania;
        {
            get { return lbl1.Text; }
            set { lbl1.Text = value; }
        }
        public bool CzySkonczone //Czy zadanie zostało zakończone;
        {
            get { return czyskonczone; }
            set
            {
                czyskonczone = value;
                if(value == true)
                {
                    button2.Text = "Skonczone";
                    button2.Enabled = false;
                }
                else
                {
                    button2.Text = "Zakoncz";
                    button2.Enabled = true;
                }
            }
        }

        public BuildingTaskView()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Otwieranie opisu zadania;
            BuildingTaskContentView opis = new BuildingTaskContentView(Nazwa, Tresc);
            opis.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Zaznaczenie, że zadanie zostało zakończone;
            CzySkonczone = true;
            DBConnection.NonQuery("UPDATE zadanie SET Czy_Skonczone = @bool WHERE " + 
                "zadanie_id = @id", new DBParameter("@bool", CzySkonczone), new DBParameter("@id", Row_Id));
        }
    }
}
