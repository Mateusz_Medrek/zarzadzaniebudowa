﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class EditBuildingTasksListView : Form
    {
        private List<NewBuildingTasksListTaskRowView> stare; //Model przed zmianami;
        public string ListName
        {
            get { return txtNameOfListaZadan.Text; }
        }
        public string ListRealizationDate
        {
            get { return dateOfListaZadan.Value.ToShortDateString(); }
        }
        public EditBuildingTasksListView(int ID)
        {
            InitializeComponent();
            ListaZadan_ID = ID; // nadajemy liście zadań jej ID w bazie;
        }
        private int ListaZadan_ID; // zmienna zawierająca ID listy zadań w bazie;
        private void ShowPracownicy()  // wyświetlenie pracowników w prawej tabeli, tej z samymi pracownikami;
        {
            #region Filling our panel with data
            //Wyszukaj wszystkich pracowników;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM pracownicy"); 
            //Wyczyść kontener;
            PracownicyTable.Controls.Clear();
            PracownicyTable.RowCount = 2;
            //Ustal co się dzieje po zajściu poniższych zdarzeń;
            PracownicyTable.ControlAdded += (s, args) => PracownicyTable.RowCount++;
            PracownicyTable.ControlRemoved += (s, args) => PracownicyTable.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego pracownika;
            {
                //Do tabeli pracownicy (tej po prawej stronie) dodajemy nową kontrolkę;
                PracownicyTable.Controls.Add(new NewBuildingTasksListWorkerRowView 
                {
                    //Nadajemy kontrolce Row_Id równe ID tego pracownika w bazie;
                    Row_Id = row.Field<int>("pracownik_id"),
                    //Nadajemy tej kontrolce zmienną Dane wartość równą Imieniu i Nazwisku pracownika;
                    Dane = row.Field<string>("pracownik_imie") + ' ' + row.Field<string>("pracownik_nazwisko") 
                }, 0, PracownicyTable.RowCount - 1);
            }
            #endregion
        }
        private void ShowZadania() // metoda wyświetlająca w lewej tabeli zadania wraz z treścią ;
        {
            #region Filling our panel with data
            //Wyszukaj zadania dla danej listy zadań;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM zadanie WHERE lista_zadan_id = " + ListaZadan_ID + " AND Czy_Skonczone = 0"); // zapytanie do bazy zadań które są zawarte w tej liście zadań i nie są skończone
            //Wyczyść kontener;
            ZadaniaTable.Controls.Clear();
            ZadaniaTable.RowCount = 10;
            //Ustal co się dzieje po zajściu poniższych zdarzeń;
            ZadaniaTable.ControlAdded += (s, args) => ZadaniaTable.RowCount++;
            ZadaniaTable.ControlRemoved += (s, args) => ZadaniaTable.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            ZadaniaPanel.HorizontalScroll.Maximum = 0;
            ZadaniaPanel.AutoScroll = false;
            ZadaniaPanel.VerticalScroll.Visible = false;
            ZadaniaPanel.AutoScroll = true;
            stare = new List<NewBuildingTasksListTaskRowView>(); //Zainicjalizuj model przed zmianami;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego zadania;
            {
                //Wstaw do modelu, który będzie modyfikowany;
                ZadaniaTable.Controls.Add(new NewBuildingTasksListTaskRowView
                {
                    // dodajemy nową kontrolkę ZadanieNowaListaZadanRow
                    Row_Id = row.Field<int>("zadanie_id"),  // pobieramy ID jakie to zadanie ma w bazie 
                    Tytul = row.Field<string>("zadanie_tytul"), // pobieramy tytuł zadania z bazy
                    Tresc = row.Field<string>("zadanie_tresc"), // pobieramy tresc zadania z bazy
                }, 0, ZadaniaTable.RowCount);
                //Wstaw do modelu przed zmianami;
                stare.Add(new NewBuildingTasksListTaskRowView
                {
                    // dodajemy nową kontrolkę ZadanieNowaListaZadanRow
                    Row_Id = row.Field<int>("zadanie_id"),  // pobieramy ID jakie to zadanie ma w bazie 
                    Tytul = row.Field<string>("zadanie_tytul"), // pobieramy tytuł zadania z bazy
                    Tresc = row.Field<string>("zadanie_tresc"), // pobieramy tresc zadania z bazy
                });
            }
            #endregion
        }
        private void ShowListaZadan_dane() // metoda pokazująca treść samej listy zadań, bez zadań;
        {
            var row = DBConnection.SelectQuery("SELECT * FROM listy_zadan WHERE lista_zadan_id = " + ListaZadan_ID).Tables[0].Rows[0];
            txtNameOfListaZadan.Text = row.Field<string>("lista_zadan_nazwa"); // pobieramy nazwę listy zadań i przekazujemy ją do textBoxa
            dateOfListaZadan.Value = row.Field<DateTime>("lista_zadan_dr"); // pobieramy datę realizacji listy zadań i przekazujemy ją do odpowiedniego elementu okna
        }
        private void EdycjaListyZadan_Load(object sender, EventArgs e)
        {
            ShowPracownicy();
            ShowZadania();
            ShowListaZadan_dane();
            showPracownicy_Zadania();
        }

        private void ZapiszButton_Click(object sender, EventArgs e)  // metoda która zapisuje listę zadań bez zadań jako nową listę zadań
        {
            if (MessageBox.Show("Czy chcesz zapisać listę zadań?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int tmp = -1;
                DateTime dt = DateTime.Now;
                #region Transakcja do BD
                try
                {
                    using (MySqlConnection conn = new MySqlConnection("server=db4free.net;database=janusz_budowa;" +
            "uid=janusz;pwd=budowaioio69;SslMode=None;old guids=true;Connection Timeout=30;"))
                    {
                        conn.Open();
                        MySqlTransaction transaction = conn.BeginTransaction();
                        try
                        {
                            using (MySqlCommand command = new MySqlCommand())
                            {
                                command.Connection = conn;
                                command.Transaction = transaction;
                                command.CommandText = "UPDATE listy_zadan SET " +
                                "lista_zadan_nazwa = @nazwa, lista_zadan_dom = @dom, lista_zadan_dr = @dr " +
                                "WHERE lista_zadan_id = @id";
                                DateTime dt2 = DateTime.Now;
                                command.Parameters.AddWithValue("@nazwa", txtNameOfListaZadan.Text);
                                command.Parameters.AddWithValue("@dom", dt2.Year + "-" + dt2.Month + "-" + dt2.Day);
                                command.Parameters.AddWithValue("@dr", dateOfListaZadan.Value.Year + "-" + dateOfListaZadan.Value.Month + "-" + dateOfListaZadan.Value.Day);
                                command.Parameters.AddWithValue("@id", ListaZadan_ID);
                                command.Prepare();
                                command.ExecuteNonQuery();
                                command.Parameters.Clear();
                                //Usuwamy ze starego modelu;
                                foreach (NewBuildingTasksListTaskRowView row in stare)
                                {
                                    command.CommandText = "DELETE FROM zadanie WHERE zadanie_id = @id";
                                    command.Parameters.AddWithValue("@id", row.Row_Id);
                                    command.Prepare();
                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                                //Dodajemy z nowego modelu;
                                foreach (NewBuildingTasksListTaskRowView row in ZadaniaTable.Controls)
                                {
                                    command.CommandText = "INSERT INTO zadanie (zadanie_tytul, zadanie_tresc, " +
                                        "lista_zadan_id, Czy_Skonczone) VALUES(@tytul, @tresc, @id, 0)";
                                    command.Parameters.AddWithValue("@tytul", row.Tytul);
                                    command.Parameters.AddWithValue("@tresc", row.Tresc);
                                    command.Parameters.AddWithValue("@id", ListaZadan_ID);
                                    command.Prepare();
                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                    command.CommandText = "SELECT MAX(zadanie_id) FROM zadanie";
                                    MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                                    DataSet ds = new DataSet();
                                    adapter.Fill(ds);
                                    int max = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                                    foreach (AssignedWorkerRowView r in row.Pracownicy)
                                    {
                                        command.CommandText = "INSERT INTO zadanie_pracownicy " +
                                            "VALUES(@zadanie_id, @pracownik_id)";
                                        command.Parameters.AddWithValue("@zadanie_id", max);
                                        command.Parameters.AddWithValue("@pracownik_id", r.Row_Id);
                                        command.Prepare();
                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                                transaction.Commit();
                                tmp = 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                transaction.Rollback();
                                MessageBox.Show("An exception of type" + ex.GetType() + " was encountered while " +
                                        "inserting the data." + ex.Message + "Neither record was written to database.");
                                Debug.WriteLine(ex.Message);
                            }
                            catch (MySqlException mex)
                            {
                                if (transaction.Connection != null)
                                {
                                    MessageBox.Show("An exception of type " + mex.GetType() + " was encountered while " +
                                        "attempting to roll back the transaction." + mex.Message);
                                    Debug.WriteLine(mex.Message);
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Coś poszło nie tak: " + ex.Message);
                }
                #endregion
                if (tmp >= 1)
                {
                    System.Media.SoundPlayer zwyciestwo = new System.Media.SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Zapisano");
                }
                this.Close();
            }
        }

        private void DodajZadanieButton_Click(object sender, EventArgs e)  // metoda dodająca nowe zadanie
        {
            ZadaniaTable.Controls.Add(new NewBuildingTasksListTaskRowView
            {
            }, 0, ZadaniaTable.RowCount);
        }
        private void showPracownicy_Zadania()  // metoda pokazująca przypisanych pracowników do odpowiednich zadań
        {
            foreach (NewBuildingTasksListTaskRowView c in ZadaniaTable.Controls)  // dla każdego zadania 'c' z prawej tabeli
            {

                DataSet ds = DBConnection.SelectQuery("SELECT pracownik_imie, pracownik_nazwisko, pracownik_id FROM pracownicy INNER JOIN zadanie_pracownicy USING (pracownik_id) WHERE zadanie_pracownicy.zadanie_id = " + c.Row_Id); // pobieramy z bazy dane pracowników którzy są przypisani do danego zadania 
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    c.Pracownicy.Add(new AssignedWorkerRowView
                    {
                        Dane = row.Field<string>("pracownik_imie") + ' ' + row.Field<string>("pracownik_nazwisko"), //pobieramy Imię i naziwkso pracownika z bazy
                        Row_Id = row.Field<int>("pracownik_id")  // pobieramy ID pracownika z bazy
                    });
                }
            }
        }

        private void SzukajTextBox_TextChanged(object sender, EventArgs e)
        {
            if (SzukajTextBox.Text == "")
            {
                foreach (NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
                {
                    r.Visible = true;
                }
                return;
            }
            foreach (NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
            {
                string nazwa = r.Dane.ToLower();
                if (nazwa.Contains(SzukajTextBox.Text.ToLower())) r.Visible = true;
                else r.Visible = false;
            }
        }
    }
}
