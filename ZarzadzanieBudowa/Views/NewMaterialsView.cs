﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewMaterialsView : Form, INewMaterialsView
    {
        private NewMaterialsController controller; //Kontroler dla widoku;

        #region ComboBox properties;
        public DataTable Categories //Kategorie materiałów;
        {
            get { return cmbbx1.DataSource as DataTable; }
            set { cmbbx1.DataSource = value; }
        }

        public string CategoriesDisplayMember //To co jest wyświetlane w ComboBoxie;
        {
            get { return cmbbx1.DisplayMember; }
            set { cmbbx1.DisplayMember = value; }
        }

        public string CategoriesValueMember //To co ComboBox zwraca jako wartość;
        {
            get { return cmbbx1.ValueMember; }
            set { cmbbx1.ValueMember = value; }
        }

        public object CategoriesSelectedValue //Wybrana wartość z ComboBoxa;
        {
            get { return cmbbx1.SelectedValue; }
        }
        #endregion

        public string Description //Opis materiałów;
        {
            get { return rtbx1.Text; }
            set { rtbx1.Text = value; }
        }

        public string Quantity //Ilość materiałów;
        {
            get { return tbx1.Text; }
            set { tbx1.Text = value; }
        }

        public string Unit
        {
            get { return lbl7.Text; }
            set { lbl7.Text = value; }
        }

        public NewMaterialsView()
        {
            InitializeComponent();
            //Ustaw opis wyświetlany po najechaniu na przycisk;
            toolTip1.SetToolTip(btn1, "Skrót: Ctrl + D");
        }

        public void SetController(NewMaterialsController controller) //Ustaw kontroler dla widoku;
        {
            this.controller = controller;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Jeśli nie podano treści, nie zapisuj;
            if (rtbx1.Text == String.Empty) MessageBox.Show("Podaj opis nowego materiału!");
            else if (tbx1.Text == String.Empty) MessageBox.Show("Podaj ilość nowego materiału!");
            else
            {
                //Wyrażenie regularne dla ilości materiałów w formacie xxxx,xx;
                string pattern = @"^[\d]{1,4}([\,\.]{1}[\d]{1,2}){0,1}$";
                Match match = Regex.Match(Quantity, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("Podano ilość w nieprawidłowym formacie! Podaj ilość w formacie xxxx,xx");
                    return;
                }

                if (MessageBox.Show("Czy zapisać zmiany?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Jeśli użytkownik potwierdzi chęć zapisania w BD;
                    int tmp = -1;
                    try
                    {
                        //Zamień wszystkie przecinki na kropki, kontroler dodaje do modelu, BD i widoku;
                        tbx1.Text = tbx1.Text.Replace('.', ',');
                        tmp = controller.AddToDB();
                        if (tmp >= 1)
                        {
                            SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                            zwyciestwo.Play();
                            MessageBox.Show("Zapisano");
                            this.Close(); //Zamykamy TO okno;
                        }
                        else MessageBox.Show("Coś poszło nie tak");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            
        }

        private void cmbbx1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (CategoriesSelectedValue is int) //Wykonaj tylko jeśli zwracana wartość jest int;
                controller.ChangeUnit(Convert.ToInt32(CategoriesSelectedValue)); //Zmień jednostkę;
        }
    }
}
