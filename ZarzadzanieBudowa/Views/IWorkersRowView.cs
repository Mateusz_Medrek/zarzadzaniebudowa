﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Views
{
    public interface IWorkersRowView
    {
        int Id { get; set; }
        string WorkerName { get; set; }
        string WorkerSurname { get; set; }
        string Pesel { get; set; }
        string EmploymentDate { get; set; }
        string Status { get; set; }
        void Dispose();
    }
}
