﻿namespace ZarzadzanieBudowa.Views
{
    partial class LoadControlView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SzukajTextBox = new System.Windows.Forms.TextBox();
            this.PracownicyTable = new System.Windows.Forms.TableLayoutPanel();
            this.PracownicyPanel = new System.Windows.Forms.Panel();
            this.PracownicyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SzukajTextBox
            // 
            this.SzukajTextBox.BackColor = System.Drawing.Color.LightBlue;
            this.SzukajTextBox.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SzukajTextBox.Location = new System.Drawing.Point(12, 12);
            this.SzukajTextBox.Name = "SzukajTextBox";
            this.SzukajTextBox.Size = new System.Drawing.Size(470, 39);
            this.SzukajTextBox.TabIndex = 1;
            this.SzukajTextBox.TextChanged += new System.EventHandler(this.SzukajTextBox_TextChanged);
            // 
            // PracownicyTable
            // 
            this.PracownicyTable.AutoSize = true;
            this.PracownicyTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PracownicyTable.ColumnCount = 1;
            this.PracownicyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.PracownicyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.PracownicyTable.Location = new System.Drawing.Point(0, 0);
            this.PracownicyTable.Margin = new System.Windows.Forms.Padding(0);
            this.PracownicyTable.MaximumSize = new System.Drawing.Size(450, 0);
            this.PracownicyTable.MinimumSize = new System.Drawing.Size(450, 39);
            this.PracownicyTable.Name = "PracownicyTable";
            this.PracownicyTable.RowCount = 2;
            this.PracownicyTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.PracownicyTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.PracownicyTable.Size = new System.Drawing.Size(450, 39);
            this.PracownicyTable.TabIndex = 0;
            // 
            // PracownicyPanel
            // 
            this.PracownicyPanel.AutoScroll = true;
            this.PracownicyPanel.BackColor = System.Drawing.Color.LightBlue;
            this.PracownicyPanel.Controls.Add(this.PracownicyTable);
            this.PracownicyPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PracownicyPanel.Location = new System.Drawing.Point(12, 57);
            this.PracownicyPanel.Name = "PracownicyPanel";
            this.PracownicyPanel.Size = new System.Drawing.Size(470, 381);
            this.PracownicyPanel.TabIndex = 19;
            // 
            // PrzegladanieObciazenia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(496, 450);
            this.Controls.Add(this.PracownicyPanel);
            this.Controls.Add(this.SzukajTextBox);
            this.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Name = "PrzegladanieObciazenia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Przeglądanie obciążenia pracowników";
            this.Load += new System.EventHandler(this.NowaListaZadan_Load);
            this.PracownicyPanel.ResumeLayout(false);
            this.PracownicyPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SzukajTextBox;
        private System.Windows.Forms.TableLayoutPanel PracownicyTable;
        private System.Windows.Forms.Panel PracownicyPanel;
    }
}