﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public partial class MaterialsView : Form, IMaterialsView
    {
        private MaterialsController controller; //Kontroler dla widoku;

        public List<IMaterialsRowView> MaterialsRowViews { get; set; } //Zbiór wyświetlanych materiałów;
        public List<string> Categories { get; set; } //Zbiór możliwych kategorii materiałów;

        public MaterialsView()
        {
            InitializeComponent();
            MaterialsRowViews = new List<IMaterialsRowView>(); //Ustaw zbiór materiałów;
            Categories = new List<string>(); //Ustaw zbiór kategorii;
            //Ustaw co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            pnl1.HorizontalScroll.Maximum = 0;
            pnl1.AutoScroll = false;
            pnl1.VerticalScroll.Visible = false;
            pnl1.AutoScroll = true;
        }

        public void SetController(MaterialsController controller)
        {
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public void AddMaterialsToView(Materials materials) //Dodaj materiały do widoku;
        {
            //Dodaj materiałów do zbioru materiałów;
            MaterialsRowViews.Add(new MaterialsRow(controller)
            {
                Id = materials.Id,
                Category = materials.Category,
                Description = materials.Description,
                Quantity = materials.Quantity,
                Unit = materials.Unit
            });
            //Dodaj do widoku;
            tlp1.Controls.Add((Control)MaterialsRowViews.Last(), 0, tlp1.RowCount);
        }

        public void ClearView() //Wyczyść widok;
        {
            //Wyczyść nasz kontener;
            tlp1.Controls.Clear();
            //Ustaw ilość wierszy w kontenerze;
            tlp1.RowCount = 2;
        }

        public void RemoveMaterialsFromView(Materials materials) //Usuń materiały z widoku;
        {
            foreach (IMaterialsRowView row in MaterialsRowViews) //Dla wszystkich materiałów;
            {
                if (row.Id == materials.Id) //Jeśli znajdziesz szukane materiały;
                {
                    MaterialsRowViews.Remove(row); //Usuń z listy;
                    break;
                }
            }
        }

        private CheckedListBox Filter()
        {
            //Stwórz nowy CheckedListBox;
            CheckedListBox filter = new CheckedListBox
            {
                BackColor = pnl1.BackColor,
                BorderStyle = BorderStyle.None,
                CheckOnClick = true,
                Font = new Font("Consolas", 12),
                Location = new Point(5, 5)
            };
            foreach (string s in Categories) filter.Items.Add(s, true);
            filter.ItemCheck += Filter_ItemCheck;
            filter.Size = new Size(278, filter.ItemHeight * filter.Items.Count);
            return filter; //Ustaw właściwości kontrolki, przypisz do niej dane i zwróć ją;
        }

        private void Filter_ItemCheck(object sender, ItemCheckEventArgs e)
        {//Metoda wykonująca się po zaznaczeniu elementu w kontrolce CheckedListBox;
            CheckedListBox filter = sender as CheckedListBox;
            foreach (MaterialsRow r in tlp1.Controls)
            {
                //Dla każdego wyświetlanego wiersza z danymi materiałów ustaw jego widoczność;
                if (e.NewValue == CheckState.Unchecked &&
                filter.SelectedItem.ToString() == r.Category) r.Visible = false;
                else if (e.NewValue == CheckState.Checked &&
                filter.SelectedItem.ToString() == r.Category) r.Visible = true;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            this.Hide(); //Schowaj TO okno;
            //Stwórz nowe okno z formularzem i dodawaniem zamówienia do BD;
            NewMaterialsView newMaterials = new NewMaterialsView();
            //Stwórz kontroler dla tego okna;
            NewMaterialsController newMaterialsController = new NewMaterialsController(newMaterials);
            //Ładuj ComboBoxa;
            newMaterialsController.LoadComboBox();
            if (newMaterialsController.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            //Kiedy okno z formularzem do dodawania materiałów zostanie zamknięte, to odśwież listę 
            //wyświetlanych materiałów, oraz pokaż z powrotem TO okno;
            newMaterials.FormClosed += (s, args) => { controller.LoadData(); this.Show(); };
            newMaterials.Show(); //Pokaż okno z formularzem;
        }

        private void btn2_Click(object sender, EventArgs e)
        {//Metoda rozwijająca(zwijająca) filtr wyszukiwania materiałów;
            pnl3.Controls[2].Visible = !pnl3.Controls[2].Visible;
            if (pnl3.Controls[2].Visible)
            {//Jeśli filtr ma być rozwinięty, to go rozwiń i zmień ikonę na strzałkę w górę;
                pnl3.Height = 38 + pnl3.Controls[2].Height;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
            }
            else
            {//Jeśli ma byćzwinięty, to go zwiń i zmień ikonę na strzałkę w dół;
                pnl3.Height = 38;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
            }
        }

        private void MaterialsView_Load(object sender, EventArgs e)
        {
            Panel panel = new Panel //Panel który będzie zawierał nasz CheckedListBox i nasz padding;
            {
                BackColor = this.BackColor,
                Location = new Point(0, 39),
                Padding = new Padding(5),
                Visible = false
            };
            panel.Controls.Add(Filter()); //Dodaj CheckedListBox wypełniony pobranymi z BD danymi;
            panel.Size = new Size(288, panel.Controls[0].Height + 10);
            pnl3.Controls.Add(panel); 
        }
    }
}
