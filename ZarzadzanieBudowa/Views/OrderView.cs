﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public partial class OrderView : Form, IOrderView
    {
        private OrderController controller; //Kontroler dla widoku;

        public List<IOrderRowView> OrderRowViews { get; set; } //Zbiór wyświetlanych zamówień;
        public List<string> Categories { get; set; } //Zbiór możliwych statusów zamówień;

        public OrderView()
        {
            InitializeComponent();
            OrderRowViews = new List<IOrderRowView>(); //Ustaw zbiór zamówień;
            Categories = new List<string>(); //Ustaw możliwe statusy;
            //Ustaw co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            pnl1.HorizontalScroll.Maximum = 0;
            pnl1.AutoScroll = false;
            pnl1.VerticalScroll.Visible = false;
            pnl1.AutoScroll = true;
        }

        public void SetController(OrderController controller) //Ustaw kontroler dla widoku;
        {
            this.controller = controller;
        }

        public void AddOrderToView(Order order) //Dodaj zamówienie do widoku;
        {
            //Dodaj zamówienie do zbioru zamówień;
            OrderRowViews.Add(new OrderRow(controller)
            {
                Id = order.Id,
                CreateDate = order.CreateDate,
                Description = order.Description,
                StatusId = order.StatusId,
                Status = order.Status
            });
            //Dodaj do widoku;
            tlp1.Controls.Add((Control)OrderRowViews.Last(), 0, tlp1.RowCount - 1);
        }

        public void ClearView() //Wyczyść widok;
        {
            //Wyczyść nasz kontener;
            tlp1.Controls.Clear();
            //Ustaw ilość wierszy;
            tlp1.RowCount = 2;
        }

        public void RemoveOrderFromView(Order order) //Usuń zamówienie z widoku;
        {
            foreach (IOrderRowView row in OrderRowViews) //Dla wszystkich zamówień;
            {
                if (row.Id == order.Id) //Jeśli znajdziesz szukane zamówienie;
                {
                    OrderRowViews.Remove(row); //Usuń ze zbioru zamówień;
                    break;
                }
            }
        }

        private CheckedListBox Filter()
        {
            //Stwórz nowy CheckedListBox;
            CheckedListBox filter = new CheckedListBox
            {
                BackColor = pnl1.BackColor,
                BorderStyle = BorderStyle.None,
                CheckOnClick = true,
                Font = new Font("Consolas", 12),
                Location = new Point(5, 5)
            };
            foreach (string s in Categories) filter.Items.Add(s, true);
            filter.ItemCheck += Filter_ItemCheck;
            filter.Size = new Size(278, filter.ItemHeight * filter.Items.Count + 5);
            return filter; //Ustaw jego właściwości i wypełnij go pobranymi danymi;
        }

        private void Filter_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //Metoda wywołująca się po zaznaczeniu elementu w CheckedListBox;
            CheckedListBox filter = sender as CheckedListBox;
            foreach (OrderRow r in tlp1.Controls)
            {
                //Ustaw widoczność materiałów w zależności od zaznaczonych kategorii;
                if (e.NewValue == CheckState.Unchecked &&
                filter.SelectedItem.ToString() == r.Status) r.Visible = false;
                else if (e.NewValue == CheckState.Checked &&
                filter.SelectedItem.ToString() == r.Status) r.Visible = true;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            this.Hide(); //Schowaj TO okno;
            //Stwórz nowe okno z formularzem do dodawania zamowien do BD;
            NewOrderView newOrderView = new NewOrderView();
            //Stwórz kontroler dla tego okna;
            NewOrderController newOrderController = new NewOrderController(newOrderView);
            //Przy zamknięciu okna z formularzem, odśwież listę wyświetlanych sprzętów oraz 
            //pokaż z powrotem TO okno;
            newOrderView.FormClosed += (s, args) => { controller.LoadData(); this.Show(); };
            newOrderView.Show(); //Pokazujemy okno z formularzem;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            pnl3.Controls[2].Visible = !pnl3.Controls[2].Visible; //Zmień widoczność;
            if (pnl3.Controls[2].Visible)
            {
                //Jeśli filtr ma być rozwinięty, to go rozwiń i zmień ikonę na strzałkę w górę;
                pnl3.Height = 38 + pnl3.Controls[2].Height;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
            }
            else
            {
                //Jeśli filtr ma być zwinięty, to go zwiń i zmień ikonę na strzałkę w dół;
                pnl3.Height = 38;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
            }
        }

        private void OrderView_Load(object sender, EventArgs e)
        {
            Panel panel = new Panel //Panel który będzie zawierał nasz CheckedListBox i nasz padding;
            {
                BackColor = this.BackColor,
                Location = new Point(0, 39),
                Padding = new Padding(5),
                Visible = false
            };
            panel.Controls.Add(Filter()); //Dodaj CheckedListBox wypełniony pobranymi z BD danymi;
            panel.Size = new Size(288, panel.Controls[0].Height + 10);
            pnl3.Controls.Add(panel);
        }
    }
}
