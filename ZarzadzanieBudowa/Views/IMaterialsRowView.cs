﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Views
{
    public interface IMaterialsRowView
    {
        int Id { get; set; } //Id materiałów w BD;
        string Category { get; set; } //Kategoria materiałów;
        string Description { get; set; } //Opis materiałów;
        string Quantity { get; set; } //Ilość materiałów;
        string Unit { get; set; } //Jednostka materiałów (zależna od kategorii);
        void Dispose(); //Zwolnij zasoby;
    }
}
