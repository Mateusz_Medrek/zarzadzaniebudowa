﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewEquipmentView : Form, INewEquipmentView
    {
        private NewEquipmentController controller; //Kontroler dla tego widoku;

        #region ComboBox properties;
        public DataTable Categories //Kategorie sprzętów;
        {
            get { return cmbbx1.DataSource as DataTable; }
            set { cmbbx1.DataSource = value; }
        }

        public string CategoriesDisplayMember //To co jest wyświetlane w ComboBoxie;
        {
            get { return cmbbx1.DisplayMember; }
            set { cmbbx1.DisplayMember = value; }
        }

        public string CategoriesValueMember //To co ComboBox zwraca jako wartość;
        {
            get { return cmbbx1.ValueMember; }
            set { cmbbx1.ValueMember = value; }
        }

        public object CategoriesSelectedValue //Wybrana wartość z ComboBoxa;
        {
            get { return cmbbx1.SelectedValue; }
        }
        #endregion

        public string Description //Opis sprzętu;
        {
            get { return rtbx1.Text; }
            set { rtbx1.Text = value; }
        }

        public NewEquipmentView()
        {
            InitializeComponent();
        }

        public void SetController(NewEquipmentController controller) //Ustaw kontroler dla widoku;
        {
            this.controller = controller;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Jeśli nie podano treści, nie zapisuj;
            if (rtbx1.Text == String.Empty) MessageBox.Show("Podaj opis nowego sprzętu!");
            else
            {
                if (MessageBox.Show("Czy chcesz zapisać zmiany?", "",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {//Jeśli użytkownik potwierdzi chęć zapisania;
                    int tmp = -1;
                    //Kontroler dodaje sprzęt do modelu, BD i widoku;
                    tmp = controller.AddToDB(this);
                    if (tmp >= 1)
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Zapisano");
                        this.Close(); //Zamknij TO okno;
                    }
                    else MessageBox.Show("Coś poszło nie tak");
                }
            }
            
        }
    }
}
