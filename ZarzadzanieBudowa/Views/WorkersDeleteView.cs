﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersDeleteView : Form, IWorkersView
    {
        private WorkerController controller; //Kontroler dla widoku;

        public List<IWorkersRowView> WorkersRowViews { get; set; } //Zbiór wyświetlanych pracowników;
        public List<string> Categories { get; set; } //Zbiór możliwych statusów pracownika;

        public WorkersDeleteView()
        {
            InitializeComponent();
            WorkersRowViews = new List<IWorkersRowView>(); //Ustaw zbiór pracowników;
            Categories = new List<string>(); //Ustaw zbiór statusów;
            //Ustaw co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            pnl1.HorizontalScroll.Maximum = 0;
            pnl1.AutoScroll = false;
            pnl1.VerticalScroll.Visible = false;
            pnl1.AutoScroll = true;
        }

        public void SetController(WorkerController controller)
        {
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public void AddWorkerToView(Worker worker)
        {
            //Dodaj pracownika do zbioru pracowników;
            WorkersRowViews.Add(new WorkersDeleteRow(controller)
            {
                Id = worker.Id,
                WorkerName = worker.WorkerName,
                WorkerSurname = worker.WorkerSurname,
                Pesel = worker.Pesel,
                EmploymentDate = worker.EmploymentDate,
                Status = worker.Status
            });
            //Dodaj do widoku;
            tlp1.Controls.Add((Control)WorkersRowViews.Last(), 0, tlp1.RowCount);
        }

        public void ClearView()
        {
            //Wyczyść nasz kontener;
            tlp1.Controls.Clear();
            //Ustaw ilość wierszy w kontenerze;
            tlp1.RowCount = 2;
        }

        public void RemoveWorkerFromView(Worker worker) //Usuń pracownika z widoku;
        {
            foreach (IWorkersRowView row in WorkersRowViews) //Dla wszystkich pracowników;
            {
                if (row.Id == worker.Id) //Jeśli znajdziesz pracownika;
                {
                    WorkersRowViews.Remove(row); //Usuń z listy;
                    break;
                }
            }
        }

        private CheckedListBox Filter()
        {
            //Stwórz nowy CheckedListBox i ustaw jego właściwości, elementy i zdarzenia;
            CheckedListBox filter = new CheckedListBox
            {
                BackColor = pnl1.BackColor,
                BorderStyle = BorderStyle.None,
                CheckOnClick = true,
                Font = new Font("Consolas", 12),
                Location = new Point(5, 5)
            };
            foreach (string s in Categories) filter.Items.Add(s, true);
            filter.ItemCheck += Filter_ItemCheck;
            filter.Size = new Size(278, filter.ItemHeight * filter.Items.Count);
            //Zwróć CheckedListBox;
            return filter;
        }

        private void Filter_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //Zdarzenie zaznaczenia (odznaczenia) elementu w CheckedListBoxie;
            CheckedListBox filter = sender as CheckedListBox;
            foreach (WorkersDeleteRow r in tlp1.Controls)
            {
                //Jeśli odznaczony to ukryj, jeśli zaznaczony to pokaż;
                if (e.NewValue == CheckState.Unchecked &&
                filter.SelectedItem.ToString() == r.Status) r.Visible = false;
                else if (e.NewValue == CheckState.Checked &&
                filter.SelectedItem.ToString() == r.Status) r.Visible = true;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //Zmień widoczność CheckedListBoxa;
            pnl3.Controls[2].Visible = !pnl3.Controls[2].Visible;
            if (pnl3.Controls[2].Visible) //Jeśli widoczny, to pokaż filtr i zmień ikonę na strzałkę w górę;
            {
                pnl3.Height = 38 + pnl3.Controls[2].Height;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
            }
            else //Jeśli nie widoczny, zwiń filtr i zmień ikonę na strzałkę w dół;
            {
                pnl3.Height = 38;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
            }
        }

        
        private void WorkersDelete_Load(object sender, EventArgs e)
        {
            Panel panel = new Panel //Panel który będzie zawierał nasz CheckedListBox i nasz padding;
            {
                BackColor = this.BackColor,
                Location = new Point(0, 39),
                Padding = new Padding(5),
                Visible = false
            };
            panel.Controls.Add(Filter()); //Dodaj CheckedListBox wypełniony pobranymi z BD danymi;
            panel.Size = new Size(288, panel.Controls[0].Height + 10);
            pnl3.Controls.Add(panel);
        }
    }
}