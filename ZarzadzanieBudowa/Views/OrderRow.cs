﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class OrderRow : UserControl, IOrderRowView
    {
        private bool isExpanded; //Zmienna określająca czy wiersz danych jest rozwinięty;
        private int statusId; //Id statusu zamówienia;
        private OrderController controller; //Kontroler dla widoku;

        public int Id { get; set; } //Id materiałów;

        public string CreateDate //Data utworzenia zamówienia;
        {
            get { return lbl1.Text; }
            set { lbl1.Text = value; }
        }

        public int StatusId //ID statusu zamówienia;
        {
            get { return statusId; }
            set
            {
                statusId = value;
                if (value == 5) btn1.Enabled = btn2.Enabled = false;
            }
        }

        public string Status //Status zamówienia;
        {
            get { return lbl2.Text; }
            set { lbl2.Text = value; }
        }

        public string Description //Opis zamówienia;
        {
            get { return rtbx1.Text; }
            set { rtbx1.Text = value; }
        }

        public OrderRow(OrderController controller)
        {
            InitializeComponent();
            isExpanded = false; //Domyślnie wiersz ma być zwinięty;
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        private void rtbx1_ContentsResized(object sender, ContentsResizedEventArgs e)
        {//Rozszerzanie textboxa tak aby dopasowywał się do swojej zawartości;
            rtbx1.Size = new Size(e.NewRectangle.Width, e.NewRectangle.Height + 5);
        }

        private void btn1_Click(object sender, EventArgs e)
        {//Metoda rozwijająca(zwijająca) wiersz danych;
            if (!isExpanded) //Jeśli wiersz nie jest rozwinięty, to go rozwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
                lbl2.Visible = false;
                isExpanded = rtbx1.Visible = true;
                this.Height = rtbx1.Height;
            }
            else //Jeśli wiersz jest rozwinięty to go zwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
                lbl2.Visible = true;
                isExpanded = rtbx1.Visible = false;
                this.Height = 39;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {//Metoda edytująca zamówienie;
            if (StatusId >= 3)
            {//Jeśli status zmienił się na wysłane lub dostarczone, nie edytuj tego zamówienia;
                MessageBox.Show("Nie możesz już edytować tego zamówienia.");
            }
            else
            {
                //Stwórz nowe okno z formularzem do edycji zamówienia, przekaż parametry zamówienia;
                NewOrderView editOrderView = new NewOrderView(Id, Description);
                //Stwórz kontroler dla tego okna;
                NewOrderController newOrderController = new NewOrderController(editOrderView);
                //Gdy okno z formularzem się zamknie, przypisz nową treść do widoku i uaktualnij model i DB;
                editOrderView.FormClosed += (s, args) =>
                {
                    Description = editOrderView.Description;
                    controller.UpdateModelValues(this);
                };
                editOrderView.Show(); //Pokaż okno;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {//Metoda usuwająca zamówienie;
            if (StatusId >= 3)
            {//Jeśli status zmienił się na wysłane lub dostarczone, nie usuwaj tego zamówienia;
                MessageBox.Show("Nie możesz już anulować tego zamówienia.");
            }
            else if (MessageBox.Show("Czy na pewno chcesz anulować zamówienie?", "",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {//Jeśli użytkownik potwierdzi chęć usunięcia;
                int tmp = -1;
                //Kontroler usuwa zamówienie z modelu, BD i widoku;
                tmp = controller.CancelOrder(this);
                if (tmp >= 1)
                {
                    SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Anulowano zamówienie");
                }
                else MessageBox.Show("Coś poszło nie tak");
            }
        }
    }
}
