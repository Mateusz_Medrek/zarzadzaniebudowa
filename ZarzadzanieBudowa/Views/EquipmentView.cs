﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public partial class EquipmentView : Form, IEquipmentView
    {
        private EquipmentController controller; //Kontroler dla widoku;

        public List<IEquipmentRowView> EquipmentRowViews { get; set; } //Zbiór wyświetlanych sprzętów;
        public List<string> Categories { get; set; } //Zbiór kategorii sprzętów;

        public EquipmentView()
        {
            InitializeComponent();
            EquipmentRowViews = new List<IEquipmentRowView>(); //Ustaw zbiór sprzętów;
            Categories = new List<string>(); //Ustaw zbiór kategorii;
            //Ustaw co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            pnl1.HorizontalScroll.Maximum = 0;
            pnl1.AutoScroll = false;
            pnl1.VerticalScroll.Visible = false;
            pnl1.AutoScroll = true;
        }

        public void SetController(EquipmentController controller)
        {
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public void AddEquipmentToView(Equipment equipment) //Dodaj sprzęt do widoku;
        {
            //Dodaj sprzęt do zbioru sprzętów;
            EquipmentRowViews.Add(new EquipmentRow(controller)
            {
                Id = equipment.Id,
                Category = equipment.Category,
                Description = equipment.Description,
                IsAvailable = equipment.IsAvailable
            });
            //Dodaj do widoku;
            tlp1.Controls.Add((Control)EquipmentRowViews.Last(), 0, tlp1.RowCount - 1);
        }

        public void ClearView() //Wyczyść widok;
        {
            //Wyczyść nasz kontener;
            tlp1.Controls.Clear();
            //Ustaw ilość wierszy;
            tlp1.RowCount = 2;
        }

        public void RemoveEquipmentFromView(Equipment equipment) //Usuń sprzęt z widoku;
        {
            foreach (IEquipmentRowView row in tlp1.Controls) //Dla wszystkich sprzętów;
            {
                if (row.Id == equipment.Id) //Jeśli znajdziesz szukany sprzęt;
                {
                    EquipmentRowViews.Remove(row); //Usuń z listy;
                    break;
                }
            }
        }

        private CheckedListBox Filter()
        {
            //Stwórz nowy CheckedListBox;
            CheckedListBox filter = new CheckedListBox
            {
                BackColor = pnl1.BackColor,
                BorderStyle = BorderStyle.None,
                CheckOnClick = true,
                Font = new Font("Consolas", 12),
                Location = new Point(5, 5)
            };
            foreach (string s in Categories) filter.Items.Add(s, true);
            filter.ItemCheck += Filter_ItemCheck;
            filter.Size = new Size(278, filter.ItemHeight * filter.Items.Count);
            return filter; //Ustaw jego właściwości i wypełnij go pobranymi danymi;
        }

        private void Filter_ItemCheck(object sender, ItemCheckEventArgs e)
        {//Metoda wywołująca się po zaznaczeniu elementu w CheckedListBox;
            CheckedListBox filter = sender as CheckedListBox;
            foreach (EquipmentRow r in tlp1.Controls)
            {
                //Ustaw widoczność materiałów w zależności od zaznaczonych kategorii;
                if (e.NewValue == CheckState.Unchecked &&
                filter.SelectedItem.ToString() == r.Category) r.Visible = false;
                else if (e.NewValue == CheckState.Checked &&
                filter.SelectedItem.ToString() == r.Category) r.Visible = true;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Stwórz nowe okno z formularzem do dodawania sprzetu do BD;
            NewEquipmentView newEquipmentView = new NewEquipmentView();
            //Stwórz kontroler dla tego okna;
            NewEquipmentController newEquipmentController = new NewEquipmentController(newEquipmentView);
            //Ładuj ComboBoxa;
            newEquipmentController.LoadComboBox();
            if (newEquipmentController.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            //Przy zamknięciu okna z formularzem, odśwież listę wyświetlanych sprzętów;
            newEquipmentView.FormClosed += (s, args) => controller.LoadData(); //Odśwież;
            newEquipmentView.Show(); //Pokaż okno z formularzem;
        }

        private void btn2_Click(object sender, EventArgs e)
        {//Metoda rozwijająca(zwijająca) filtr;
            pnl3.Controls[2].Visible = !pnl3.Controls[2].Visible; //Zmień widoczność;
            if (pnl3.Controls[2].Visible)
            {//Jeśli filtr ma być rozwinięty, to go rozwiń i zmień ikonę na strzałkę w górę;
                pnl3.Height = 38 + pnl3.Controls[2].Height;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
            }
            else
            {//Jeśli filtr ma być zwinięty, to go zwiń i zmień ikonę na strzałkę w dół;
                pnl3.Height = 38;
                btn2.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
            }
        }

        private void EquipmentView_Load(object sender, EventArgs e)
        {
            Panel panel = new Panel //Panel który będzie zawierał nasz CheckedListBox i nasz padding;
            {
                BackColor = this.BackColor,
                Location = new Point(0, 39),
                Padding = new Padding(5),
                Visible = false
            };
            panel.Controls.Add(Filter()); //Dodaj CheckedListBox wypełniony pobranymi z BD danymi;
            panel.Size = new Size(288, panel.Controls[0].Height + 10);
            pnl3.Controls.Add(panel);
        }
    }
}
