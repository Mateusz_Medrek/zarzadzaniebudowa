﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WarehouseView : Form
    {
        public WarehouseView()
        {
            InitializeComponent();
            //Ustaw opisy pokazujące się po najechaniu na przyciski;
            toolTip1.SetToolTip(btn1, "Skrót: Ctrl + 1");
            toolTip1.SetToolTip(btn2, "Skrót: Ctrl + 2");
            toolTip1.SetToolTip(btn3, "Skrót: Ctrl + 3");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Metoda otwierająca okno z listą materiałów;
            //Schowaj TO okno;
            this.Hide();
            //Stwórz nowe okno z materiałami;
            MaterialsView materials = new MaterialsView();
            //Stwórz kontroler dla nowego okna i przypisz to okno;
            MaterialsController controller = new MaterialsController(materials);
            //Kiedy okno z materiałami zostanie zamknięte, pokaż z powrotem TO okno;
            materials.FormClosed += (s, args) => this.Show();
            materials.Show(); //Pokaż okno z materiałami;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //Analogicznie jak wyżej - okno z listą sprzętów;
            this.Hide();
            EquipmentView equipment = new EquipmentView();
            EquipmentController controller = new EquipmentController(equipment);
            if (controller.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak.");
                return;
            }
            equipment.FormClosed += (s, args) => this.Show();
            equipment.Show();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            //Analogicznie jak wyżej - okno z listą zamówień;
            this.Hide();
            OrderView order = new OrderView();
            OrderController controller = new OrderController(order);
            order.FormClosed += (s, args) => this.Show();
            order.Show();
        }
    }
}
