﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class BuildingTaskTitleView : UserControl
    {
        public int Row_Id { get; set; } //Id zadania;
        public string Nazwa //Nazwa zadania;
        {
            get { return lbl1.Text; }
            set { lbl1.Text = value; }
        }
        public string NazwaListy //Nazwa listy zadań, do której przydzielone jest zadanie;
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public BuildingTaskTitleView()
        {
            InitializeComponent();
        }
    }
}
