﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class AssignedWorkerRowView : UserControl
    {
        public int Row_Id { get; set; } //Id przydzielonwgo pracownika;
        public string Dane //Imię i nazwisko przydzielonego pracownika;
        {
            get { return labelDanePracownika.Text; }
            set { labelDanePracownika.Text = value; }
        }

        public AssignedWorkerRowView()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
