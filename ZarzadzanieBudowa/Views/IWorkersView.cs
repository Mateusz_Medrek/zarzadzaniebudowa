﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public interface IWorkersView
    {
        List<IWorkersRowView> WorkersRowViews { get; set; }
        List<string> Categories { get; set; }
        void SetController(WorkerController controller);
        void AddWorkerToView(Worker worker);
        void ClearView();
        void RemoveWorkerFromView(Worker worker);
    }
}
