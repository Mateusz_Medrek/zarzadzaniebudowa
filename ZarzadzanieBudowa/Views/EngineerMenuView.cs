﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class EngineerMenuView : Form
    {
        public EngineerMenuView(string login = "")
        {
            InitializeComponent();
            this.Text = "Witaj: " + login;
            //Ustaw podpowiedzi wyświetające się po najechaniu na daną kontrolkę;
            toolTip1.SetToolTip(btn3, "Skrót: Ctrl + 1");
            toolTip2.SetToolTip(btn6, "Skrót: Ctrl + 2");
            toolTip3.SetToolTip(btn2, "Skrót: Ctrl + 3");
            toolTip4.SetToolTip(btn4, "Skrót: Ctrl + 4");
        }
       
        private void btn2_Click(object sender, EventArgs e)
        {
            this.Hide(); //Ukryj TO okno;
            //Stwóz nowe okno z formularzem do tworzenia listy zadań;
            NewBuildingTasksListView nowaListaZadan = new NewBuildingTasksListView();
            nowaListaZadan.FormClosed += (s, args) => this.Show(); //Ustal co sięzdarzy po zamknięciu nowego okna;
            nowaListaZadan.Show(); //Pokaż nowe okno;
        }

        private void btn3_Click(object sender, EventArgs e)
        {//Analogicznie jak wyżej - okno z Kontrolą postępu prac;
            this.Hide();
            ProgressControlView kontrola = new ProgressControlView();
            kontrola.FormClosed += (s, args) => this.Show();
            kontrola.Show();
	    }

        private void btn4_Click(object sender, EventArgs e)
        {//Analogicznie jak wyżej - okno z Przeglądaniem obciążenia pracowników;
            this.Hide();
            LoadControlView pob = new LoadControlView();
            pob.FormClosed += (s, args) => this.Show();
            pob.Show();
        }

        private void btn6_Click(object sender, EventArgs e)
        {//Analogicznie jak wyżej - okno z Magazynem;
            this.Hide();
            WarehouseView magazyn = new WarehouseView();
            magazyn.FormClosed += (s, args) => this.Show();
            magazyn.Show();
        }

        #region Skróty klawiszowe
        private void btn3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D1 && e.Modifiers == Keys.Control)
            {
                btn3_Click(this, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.D2 && e.Modifiers == Keys.Control)
            {
                btn6_Click(this, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.D3 && e.Modifiers == Keys.Control)
            {
                btn2_Click(this, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.D4 && e.Modifiers == Keys.Control)
            {
                btn4_Click(this, EventArgs.Empty);
            }
        }

        private void btn6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                btn3.Focus();
            }
        }

        private void btn2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                btn3.Focus();
            }
        }

        private void btn4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                btn3.Focus();
            }
        }
        #endregion
    }
}
