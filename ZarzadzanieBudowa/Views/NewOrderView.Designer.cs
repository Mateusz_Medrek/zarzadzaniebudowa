﻿namespace ZarzadzanieBudowa.Views
{
    partial class NewOrderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.rtbx1 = new System.Windows.Forms.RichTextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(12, 13);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(180, 96);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Podaj opis:\r\n(max 65535\r\nznaków)";
            // 
            // rtbx1
            // 
            this.rtbx1.BackColor = System.Drawing.Color.LightBlue;
            this.rtbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbx1.Location = new System.Drawing.Point(198, 12);
            this.rtbx1.MaxLength = 65535;
            this.rtbx1.Name = "rtbx1";
            this.rtbx1.Size = new System.Drawing.Size(574, 202);
            this.rtbx1.TabIndex = 1;
            this.rtbx1.Text = "";
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(18, 164);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(125, 50);
            this.btn1.TabIndex = 2;
            this.btn1.Text = "Dodaj";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // NewOrderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(784, 231);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.rtbx1);
            this.Controls.Add(this.lbl1);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinimumSize = new System.Drawing.Size(800, 270);
            this.Name = "NewOrderView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj nowe zamówienie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.RichTextBox rtbx1;
        private System.Windows.Forms.Button btn1;
    }
}