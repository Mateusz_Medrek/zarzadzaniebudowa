﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class ProgressControlRowView : UserControl
    {
        public ProgressControlRowView()
        {
            InitializeComponent();
        }

        public int Row_Id { get; set; } //Id listy zadań;
        public string Utworzenia { get; set; } //Data utworzenia listy zadań;
        public string Realizacji { get; set; } //Data realizacji listy zadań;
        public string OstatniejModyfikacji{ get; set; } //Data ostatniej modyfikacji listy zadań;
        public string Nazwa
        {
            get { return lbl1.Text; }
            set { lbl1.Text = value; }
        }
        
        private void btn1_Click(object sender, EventArgs e)
        {
            //Otwórz okno do zaznaczenia wykonanych zadań w danej liście zadań;
            CheckCompletedTasksView zaznacz = new CheckCompletedTasksView(this.Row_Id, this.Nazwa, 
                this.Utworzenia, this.Realizacji, this.OstatniejModyfikacji);
            zaznacz.Show();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //Otwóz okno z formularzem do edycji danej listy zadań;
            EditBuildingTasksListView edycjaListyZadan = new EditBuildingTasksListView(Row_Id);
            edycjaListyZadan.FormClosed += (s, args) =>
            {
                Nazwa = edycjaListyZadan.ListName;
                Realizacji = edycjaListyZadan.ListRealizationDate;
                OstatniejModyfikacji = DateTime.Now.ToShortDateString();
                this.Show();
            };
            edycjaListyZadan.Show();
        }
    }
}
