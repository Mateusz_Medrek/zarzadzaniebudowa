﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public interface IOrderView
    {
        List<IOrderRowView> OrderRowViews { get; set; } //Zbiór zamówień jako elementy graficzne;
        List<string> Categories { get; set; } //Zbiór możliwych statusów zamówienia;
        void SetController(OrderController controller); //Ustaw kontroler dla widoku;
        void AddOrderToView(Order order); //Dodaj zamówienie do widoku;
        void ClearView(); //Wyczyść widok;
        void RemoveOrderFromView(Order order); //Usuń zamówienie z widoku;
    }
}
