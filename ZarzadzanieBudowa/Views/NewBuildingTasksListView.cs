﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewBuildingTasksListView : Form
    {
        public NewBuildingTasksListView()
        {
            InitializeComponent();
        }
        private void ShowPracownicy()
        {
            #region Filling our panel with data
            //Wyszukaj wszystkich pracowników;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM pracownicy");
            //Wyczyść kontener;
            PracownicyTable.Controls.Clear();
            PracownicyTable.RowCount = 2;
            //Ustal co się dzieje po zajściu poniższych zdarzeń;
            PracownicyTable.ControlAdded += (s, args) => PracownicyTable.RowCount++;
            PracownicyTable.ControlRemoved += (s, args) => PracownicyTable.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego pracownika;
            {
                //Dodaj pracownika do kontenera;
                PracownicyTable.Controls.Add(new NewBuildingTasksListWorkerRowView
                {
                    Row_Id = row.Field<int>("pracownik_id"),
                    Dane = row.Field<string>("pracownik_imie") + ' ' + row.Field<string>("pracownik_nazwisko")
                }, 0, PracownicyTable.RowCount - 1);
            }
            #endregion
        }
        private void NowaListaZadan_Load(object sender, EventArgs e)
        {
            ShowPracownicy();  // wywołanie metody ShowPracownicy() 
            //Wyczyść kontener;
            ZadaniaTable.Controls.Clear();  
            ZadaniaTable.RowCount = 1;
            //Ustal co się dzieje po zajściu poniższych zdarzeń;
            ZadaniaTable.ControlAdded += (s, args) => ZadaniaTable.RowCount++;
            ZadaniaTable.ControlRemoved += (s, args) => ZadaniaTable.RowCount--;
            //Wyłącz horyzontalny (poziomy) pasek przewijania;
            ZadaniaPanel.HorizontalScroll.Maximum = 0;
            ZadaniaPanel.AutoScroll = false;
            ZadaniaPanel.VerticalScroll.Visible = false;
            ZadaniaPanel.AutoScroll = true;
            //Dodaj nowe zadanie do kontenera;
            ZadaniaTable.Controls.Add(new NewBuildingTasksListTaskRowView
            {
                isExpanded = true,  // dodaje puste rozwinięte zadanie
            }, 0, ZadaniaTable.RowCount - 1);
        }

        private void ZapiszButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy chcesz zapisać zlistę zadań?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int tmp = -1;
                DateTime dt = DateTime.Now;
                //Dodaj do BD listę zadań;
                tmp = DBConnection.NonQuery("INSERT INTO listy_zadan (lista_zadan_nazwa, lista_zadan_dom, " +
                    "lista_zadan_du, lista_zadan_dr)" +
                    "VALUES (@opis, @du, @du, @dr)", new DBParameter("@opis", txtNameOfListaZadan.Text),
                    new DBParameter("@du", dt.Year + "-" + dt.Month + "-" + dt.Day),
                    new DBParameter("@dr", dateOfListaZadan.Value.Year + "-" + dateOfListaZadan.Value.Month 
                    + "-" + dateOfListaZadan.Value.Day));
                //Znajdź ID dodanej bazy;
                int ID = (int)(DBConnection.SelectQuery("SELECT MAX(lista_zadan_id) " +
                    "FROM listy_zadan").Tables[0].Rows[0][0]);
                foreach(NewBuildingTasksListTaskRowView zadanie in ZadaniaTable.Controls)
                {
                    //Dodaj do BD zadanie;
                    tmp = DBConnection.NonQuery("INSERT INTO zadanie (zadanie_tytul, zadanie_tresc, " +
                        "lista_zadan_id, Czy_skonczone)" +
                        "VALUES (@tytul, @tresc, @ID, 0)", new DBParameter("@tytul", zadanie.Tytul),
                    new DBParameter("@tresc", zadanie.Tresc),
                    new DBParameter("@ID", ID));
                    //Znajdź ID dodanego zadania;
                    int id_zadania = (int)(DBConnection.SelectQuery("SELECT MAX(zadanie_id) " +
                        "FROM zadanie").Tables[0].Rows[0][0]);
                    foreach(AssignedWorkerRowView pracownik in zadanie.Pracownicy)
                    {
                        //Dodaj do BD przydzielonych pracowników;
                        tmp = DBConnection.NonQuery("INSERT INTO zadanie_pracownicy (zadanie_id, pracownik_id)" + 
                            "VALUES (@id_zadanie, @id_pracownik)",
                        new DBParameter("@id_zadanie", id_zadania),
                        new DBParameter("@id_pracownik", pracownik.Row_Id));
                    }
                }
                if (tmp >= 1)
                {
                    System.Media.SoundPlayer zwyciestwo = new System.Media.SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Zapisano");
                }
                else MessageBox.Show("Coś poszło nie tak"); 
                this.Close();
            }
        }

        private void DodajZadanieButton_Click(object sender, EventArgs e) // metoda dodająca nowe puste zadanie
        {
            ZadaniaTable.Controls.Add(new NewBuildingTasksListTaskRowView
            {
                // dodajemy nową kontrolkę OurControls.ZadanieNowaListaZadanRow do tabeli z zadaniami
            }, 0, ZadaniaTable.RowCount - 1);
        }

        private void SzukajTextBox_TextChanged(object sender, EventArgs e)
        {
            if (SzukajTextBox.Text == "")
            {
                foreach (NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
                {
                    //Dla każdego wyświetlanego wiersza z pracownikami ustaw jego widoczność;
                    r.Visible = true;
                }
                return;
            }
            foreach (NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
            {
                //Dla każdego wyświetlanego wiersza z danymi mracowników ustaw jego widoczność;
                string nazwa = r.Dane.ToLower();
                if (nazwa.Contains(SzukajTextBox.Text.ToLower())) r.Visible = true;
                else r.Visible = false;
            }
        }

    }
}
