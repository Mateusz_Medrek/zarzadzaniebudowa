﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class EditMaterialsView : Form
    {
        private MaterialsController controller; //Kontroler dla widoku;
        private MaterialsRow row; //Materialy jako kontrolka graficzna;
        private int id; //Id materiału;
        private string category; //Kategoria materiału;
        private decimal quantity; //Ilość materiału;

        private decimal Old_Quantity { get; set; }
        public decimal New_Quantity {
            get { return quantity; }
        }

        public EditMaterialsView(MaterialsController controller, MaterialsRow row, int id, 
            string category, decimal quantity)
        {
            InitializeComponent();
            this.controller = controller;
            this.row = row;
            this.id = id;
            this.category = category;
            this.quantity = quantity;
            Old_Quantity = quantity;
            nud1.Minimum = -quantity; //Maksymalnie można odjąć tyle materiałów ile jest dostępnych;
            nud1.Maximum = 9999-quantity; //Maksymalnie można mieć 9999 jednostek materiałów;
            lbl2.Text = lbl5.Text = Convert.ToString(quantity);
            lbl6.Text = lbl7.Text = lbl8.Text = String.Format("{0}", row.Unit); //Dobieramy jednostkę;
        }

        private void nud1_ValueChanged(object sender, EventArgs e)
        {
            //Metoda wywołująca podczas każdej zmiany w wartości w kontrolce NumericUpDown;
            decimal tmp = Convert.ToDecimal(lbl2.Text); //Uaktualnianie ilości;
            tmp += nud1.Value;
            lbl5.Text = Convert.ToString(tmp);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Metoda wykonująca zapytanie UPDATE lub DELETE (w zależności czy wykorzystujemy wszystkie zasoby)
            quantity = Convert.ToDecimal(lbl5.Text);
            if (quantity > 0) //Jeśli nie wykorzystujemy wszystkich zasobów danych materiałów z magazynu
            {
                if (MessageBox.Show("Czy zapisać zmiany?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Jeśli użytkownik potwierdza, uaktualniamy model i BD;
                    int tmp = -1;
                    row.Quantity = Convert.ToString(quantity);
                    tmp = controller.UpdateModelValues(row);
                    if (tmp >= 1)
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Zapisano");
                        this.Close(); //Zamykamy TO okno;
                    }
                    else
                    {
                        row.Quantity = Convert.ToString(Old_Quantity);
                        MessageBox.Show("Coś poszło nie tak");
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Czy na pewno chcesz wykorzystać wszystkie zasoby?", "", 
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Jeśli użytkownik potwierdza, usuwamy dane materialy z modelu i BD;
                    int tmp = -1;
                    //Kontroler usuwa materiały;
                    tmp = controller.RemoveMaterials(row);
                    if (tmp >= 1)
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Zapisano");
                        this.Close(); //Zamykamy TO okno;
                    }
                    else MessageBox.Show("Coś poszło nie tak");
                }
            }
        }
    }
}
