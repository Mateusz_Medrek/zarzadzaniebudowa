﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersCreateView : Form, INewEditWorkersView
    {
        private NewEditWorkerController controller; //Kontroler dla widoku;

        public int Id { get; set; } //Id nowego pracownika;

        public string WorkerName //Imię nowego pracownika;
        {
            get { return tbx1.Text; }
            set { tbx1.Text = value; }
        }

        public string WorkerSurname //Nazwisko nowego pracownika;
        {
            get { return tbx2.Text; }
            set { tbx2.Text = value; }
        }

        public string Pesel //Pesel nowego pracownika;
        {
            get { return tbx3.Text; }
            set { tbx3.Text = value; }
        }

        public DateTime EmploymentDate //Data zatrudnienia nowego pracownika;
        {
            get { return datePicker1.Value; }
            set { datePicker1.Value = value; }
        }

        public DataTable Categories //Źródło danych ComboBoxa;
        {
            get { return cmbbx1.DataSource as DataTable; }
            set { cmbbx1.DataSource = value; }
        }

        public string CategoriesDisplayMember //Dane, które są wyświetlane użytkownikowi w ComboBoxie;
        {
            get { return cmbbx1.DisplayMember; }
            set { cmbbx1.DisplayMember = value; }
        }

        public string CategoriesValueMember //Dane, które są przekazywane jako wartość z ComboBoxa;
        {
            get { return cmbbx1.ValueMember; }
            set { cmbbx1.ValueMember = value; }
        }

        public object CategoriesSelectedValue //Wybrana obecność wartość w ComboBoxie;
        {
            get { return cmbbx1.SelectedValue; }
            set { cmbbx1.SelectedValue = value; }
        }

        public WorkersCreateView()
        {
            InitializeComponent();
            //Ustaw opis wyświetlany po najechaniu na przycisk;
            toolTip1.SetToolTip(btn1, "Skrót: Ctrl + D");
        }

        public void SetController(NewEditWorkerController controller)
        {
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        private void WorkersCreate_Load(object sender, EventArgs e)
        {
            //Przy załadowaniu okna ustawiamy właściwości dla naszych kontrolek;
            datePicker1.MinDate = Convert.ToDateTime("01-01-1970");
            datePicker1.MaxDate = DateTime.Now;
            datePicker1.Value = datePicker1.MaxDate;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Jeśli nie podano treści, nie zapisuj;
            if (tbx1.Text == String.Empty || tbx2.Text == String.Empty || tbx3.Text == String.Empty)
                MessageBox.Show("Podaj wszystkie dane!");
            else //Sprawdź czy podane dane są prawidłowe, jeśli nie to nie zapisuj;
            {
                string pattern = "[A-Za-ząęćśłóźżńŻŁŚĄĘŹ]"; //Wyrażenie regularne dla imienia i nazwiska;
                Match match = Regex.Match(WorkerName, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("Imię zawiera niepoprawne znaki!");
                    return;
                }

                match = Regex.Match(WorkerSurname, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("Nazwisko zawiera niepoprawne znaki!");
                    return;
                }

                pattern = @"^\d{11}$"; //Wyrażenie regularne dla pesela;
                match = Regex.Match(Pesel, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("PESEL zawiera niepoprawne znaki!");
                    return;
                }

                if (MessageBox.Show("Czy zapisać zmiany?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Jeśli użytkownik potwierdzi chęć zapisania w BD;
                    int tmp = -1;
                    tmp = controller.AddToDB(this);
                    if (tmp >= 1) //Jeśli operacja się udała, zagraj dźwięk, pokaż komunikat;
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Zapisano!", "", MessageBoxButtons.OK);
                        //Wyczyść formularz;
                        WorkerName = WorkerSurname = Pesel = "";
                        EmploymentDate = datePicker1.MaxDate;
                    }
                    else MessageBox.Show("Coś poszło nie tak");
                }
            }
            
        }
    }
}
