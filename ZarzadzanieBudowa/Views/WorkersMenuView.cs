﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersMenuView : Form
    {
        public WorkersMenuView()
        {
            InitializeComponent();
            //Ustaw pokazywanie opisów po najechaniu na daną kontrolkę;
            toolTip1.SetToolTip(btn1, "Skrót: Ctrl + 1");
            toolTip2.SetToolTip(btn2, "Skrót: Ctrl + 2");
            toolTip3.SetToolTip(btn3, "Skrót: Ctrl + 3");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Metoda pokazująca okno z Dodawaniem pracownika;
            this.Hide(); //Schowaj TO okno;
            //Stwórz nowe okno z Dodawaniem pracownika;
            WorkersCreateView workersCreate = new WorkersCreateView();
            //Stwórz kontroler dla nowego okna i przypisz to okno;
            NewEditWorkerController controller = new NewEditWorkerController(workersCreate);
            if (controller.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            //Gdy okno z Dodawaniem pracownika się zamknie, pokaż z powrotem TO okno;
            workersCreate.FormClosed += (s, args) => this.Show();
            workersCreate.Show(); //Pokaż okno z Dodawaniem pracownika;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //Analogicznie jak wyżej - okno z Modyfikowaniem danych pracownika;
            this.Hide();
            WorkersUpdateView workersUpdate = new WorkersUpdateView();
            WorkerController controller = new WorkerController(workersUpdate);
            if (controller.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            workersUpdate.FormClosed += (s, args) => this.Show();
            workersUpdate.Show();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            //Analogicznie jak wyżej - okno z Usuwaniem pracownika;
            this.Hide();
            WorkersDeleteView workersDelete = new WorkersDeleteView();
            WorkerController controller = new WorkerController(workersDelete);
            if (controller.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            workersDelete.FormClosed += (s, args) => this.Show();
            workersDelete.Show();
        }

        #region Zdarzenia po naciśnięciu klawiszy Ctrl+(1,2,3);
        private void btn1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.D1 && e.Modifiers == Keys.Control)
            {
                btn1_Click(this, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.D2 && e.Modifiers == Keys.Control)
            {
                btn2_Click(this, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.D3 && e.Modifiers == Keys.Control)
            {
                btn3_Click(this, EventArgs.Empty);
            }
        }

        private void btn2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                btn1.Focus();
            }
        }

        private void btn3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                btn1.Focus();
            }
        }
        #endregion
    }
}
