﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class ManagerMenuView : Form
    {
        string login="";
        public ManagerMenuView(string login = "")
        {
            InitializeComponent();
            this.Text = "Witam Panie Kierowniku: " + login;
            this.login = login;
            //Podpowiedzi po najechaniu na daną kontrolkę;
            toolTip1.SetToolTip(btn6, "Skrót: Ctrl + 1");
            toolTip2.SetToolTip(btn5, "Skrót: Ctrl + 2");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            this.Hide(); //Schowaj TO okno;
            //Stwórz nowe okno z formularzem do tworzenia raportów;
            ReportView raporty = new ReportView(login);
            //Przypisz mu nowy kontroler;
            ReportController controller = new ReportController(raporty);
            raporty.FormClosed += (s, args) => this.Show(); //Ustal co się dzieje po zajściu poniższego zdarzenia;
            raporty.Show(); //Pokaż nowe okno;
        }

        private void btn6_Click(object sender, EventArgs e)
        {//Analogicznie jak wyżej - okno z Magazynem;
            this.Hide();
            WarehouseView magazyn = new WarehouseView();
            magazyn.FormClosed += (s, args) => this.Show();
            magazyn.Show();
        }
    }
}
