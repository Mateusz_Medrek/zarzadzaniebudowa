﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewBuildingTasksListWorkerRowView : UserControl
    {
        public int Row_Id { get; set; } //Id przydzielonego pracownika;
        public string Nazwa { get; set; }
        public string Dane
        {
            get { return LabelDanePrcownika.Text; }
            set { LabelDanePrcownika.Text = value; }
        }

        public NewBuildingTasksListWorkerRowView()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {   
            LoadView obciazenie = new LoadView(this.Row_Id, this.Nazwa);
            obciazenie.Show();   
        }

        private void LabelDanePrcownika_MouseDown(object sender, MouseEventArgs e)
        {
            string ID = Convert.ToString(this.Row_Id);
            LabelDanePrcownika.DoDragDrop(ID, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void LabelDanePrcownika_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
    }
}
