﻿namespace ZarzadzanieBudowa.Views
{
    partial class OrderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.tlp1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl3 = new System.Windows.Forms.Panel();
            this.btn2 = new System.Windows.Forms.Button();
            this.lbl10 = new System.Windows.Forms.Label();
            this.pnl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(712, 459);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(350, 50);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "Dodaj zamówienie";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl2.Location = new System.Drawing.Point(362, 3);
            this.lbl2.Margin = new System.Windows.Forms.Padding(3);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(180, 32);
            this.lbl2.TabIndex = 4;
            this.lbl2.Text = "Status/Opis";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(59, 3);
            this.lbl1.Margin = new System.Windows.Forms.Padding(3);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(240, 32);
            this.lbl1.TabIndex = 3;
            this.lbl1.Text = "Data utworzenia";
            // 
            // pnl1
            // 
            this.pnl1.BackColor = System.Drawing.Color.LightBlue;
            this.pnl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl1.Controls.Add(this.tlp1);
            this.pnl1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnl1.Location = new System.Drawing.Point(12, 56);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(1050, 397);
            this.pnl1.TabIndex = 8;
            // 
            // tlp1
            // 
            this.tlp1.AutoSize = true;
            this.tlp1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlp1.BackColor = System.Drawing.Color.LightBlue;
            this.tlp1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlp1.ColumnCount = 1;
            this.tlp1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tlp1.Location = new System.Drawing.Point(0, 0);
            this.tlp1.Margin = new System.Windows.Forms.Padding(0);
            this.tlp1.MinimumSize = new System.Drawing.Size(1046, 36);
            this.tlp1.Name = "tlp1";
            this.tlp1.RowCount = 2;
            this.tlp1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlp1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlp1.Size = new System.Drawing.Size(1046, 36);
            this.tlp1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl2);
            this.panel1.Controls.Add(this.lbl1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(756, 38);
            this.panel1.TabIndex = 9;
            // 
            // pnl3
            // 
            this.pnl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnl3.Controls.Add(this.btn2);
            this.pnl3.Controls.Add(this.lbl10);
            this.pnl3.Location = new System.Drawing.Point(774, 12);
            this.pnl3.Name = "pnl3";
            this.pnl3.Size = new System.Drawing.Size(288, 38);
            this.pnl3.TabIndex = 12;
            // 
            // btn2
            // 
            this.btn2.BackgroundImage = global::ZarzadzanieBudowa.Properties.Resources.ArrowDown;
            this.btn2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn2.Location = new System.Drawing.Point(228, 0);
            this.btn2.Margin = new System.Windows.Forms.Padding(0);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 38);
            this.btn2.TabIndex = 1;
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl10.Location = new System.Drawing.Point(15, 3);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(210, 32);
            this.lbl10.TabIndex = 0;
            this.lbl10.Text = "Wybierz filtr";
            // 
            // OrderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1074, 521);
            this.Controls.Add(this.pnl3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.btn1);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinimumSize = new System.Drawing.Size(1090, 560);
            this.Name = "OrderView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zarządzanie zamówieniami";
            this.Load += new System.EventHandler(this.OrderView_Load);
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnl3.ResumeLayout(false);
            this.pnl3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.TableLayoutPanel tlp1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnl3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Label lbl10;
    }
}