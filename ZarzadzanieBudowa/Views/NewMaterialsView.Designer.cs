﻿namespace ZarzadzanieBudowa.Views
{
    partial class NewMaterialsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl1 = new System.Windows.Forms.Label();
            this.cmbbx1 = new System.Windows.Forms.ComboBox();
            this.lbl2 = new System.Windows.Forms.Label();
            this.rtbx1 = new System.Windows.Forms.RichTextBox();
            this.lbl3 = new System.Windows.Forms.Label();
            this.tbx1 = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(12, 19);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(214, 24);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Wybierz kategorię";
            // 
            // cmbbx1
            // 
            this.cmbbx1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbbx1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbbx1.BackColor = System.Drawing.Color.LightBlue;
            this.cmbbx1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cmbbx1.FormattingEnabled = true;
            this.cmbbx1.Location = new System.Drawing.Point(250, 10);
            this.cmbbx1.Name = "cmbbx1";
            this.cmbbx1.Size = new System.Drawing.Size(422, 32);
            this.cmbbx1.TabIndex = 1;
            this.cmbbx1.SelectedValueChanged += new System.EventHandler(this.cmbbx1_SelectedValueChanged);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl2.Location = new System.Drawing.Point(12, 65);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(130, 24);
            this.lbl2.TabIndex = 2;
            this.lbl2.Text = "Podaj opis";
            // 
            // rtbx1
            // 
            this.rtbx1.BackColor = System.Drawing.Color.LightBlue;
            this.rtbx1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbx1.Location = new System.Drawing.Point(250, 56);
            this.rtbx1.Name = "rtbx1";
            this.rtbx1.Size = new System.Drawing.Size(422, 200);
            this.rtbx1.TabIndex = 3;
            this.rtbx1.Text = "";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl3.Location = new System.Drawing.Point(12, 278);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(166, 24);
            this.lbl3.TabIndex = 4;
            this.lbl3.Text = "Podaj ilość (";
            // 
            // tbx1
            // 
            this.tbx1.BackColor = System.Drawing.Color.LightBlue;
            this.tbx1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx1.Location = new System.Drawing.Point(309, 275);
            this.tbx1.Name = "tbx1";
            this.tbx1.Size = new System.Drawing.Size(227, 32);
            this.tbx1.TabIndex = 5;
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(400, 314);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(272, 40);
            this.btn1.TabIndex = 6;
            this.btn1.Text = "Dodaj do magazynu";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(181, 278);
            this.lbl4.Margin = new System.Windows.Forms.Padding(0);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(97, 26);
            this.lbl4.TabIndex = 7;
            this.lbl4.Text = "max do 2 miejsc\r\npo przecinku";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl5.Location = new System.Drawing.Point(281, 278);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(22, 24);
            this.lbl5.TabIndex = 8;
            this.lbl5.Text = ")";
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl7.Location = new System.Drawing.Point(542, 278);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(130, 24);
            this.lbl7.TabIndex = 9;
            this.lbl7.Text = "xxxxxxxxxx";
            // 
            // NewMaterialsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.tbx1);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.rtbx1);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.cmbbx1);
            this.Controls.Add(this.lbl1);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "NewMaterialsView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj nowe materiały";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.ComboBox cmbbx1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.RichTextBox rtbx1;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.TextBox tbx1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}