﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class MaterialsRow : UserControl, IMaterialsRowView
    {
        private bool isExpanded; //Zmienna określająca czy wiersz danych jest rozwinięty;
        private MaterialsController controller; //Kontroler dla widoku;

        public MaterialsRow(MaterialsController controller)
        {
            InitializeComponent();
            isExpanded = false; //Domyślnie wiersz ma być zwinięty;
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public int Id { get; set; } //Id materiałów;

        public string Category //Kategoria materiałów;
        {
            get { return lbl1.Text; }
            set
            {
                if (value.Length <= 16) //Jeśli długość kategorii większa niż 16,
                    lbl1.Text = value;  //to przypisz tylko pierwsze 13 znaków oraz wielokropek;
                else
                    lbl1.Text = value.Substring(0, 13) + "...";
            }
        }

        public string ShortDescription //Opis gdy wiersz danych jest zwinięty;
        {
            get { return lbl2.Text; }
            set
            {
                if (value.Length <= 16) //Analogicznie jak wyżej;
                    lbl2.Text = value;
                else
                    lbl2.Text = value.Substring(0, 13) + "...";
            }
        }

        public string Description //Opis gdy wiersz danych jest rozwinięty;
        {
            get { return rtbx1.Text; }
            set
            {
                rtbx1.Text = value;
                ShortDescription = value;
            }
        }

        public string Quantity //Ilość materiałów;
        {
            get { return lbl3.Text; }
            set { lbl3.Text = value; }
        }

        public string Unit
        {
            get { return lbl4.Text; }
            set { lbl4.Text = value; }
        }

        private void rtbx1_ContentsResized(object sender, ContentsResizedEventArgs e)
        {//Rozszerzanie textboxa tak aby dopasowywał się do swojej zawartości;
            rtbx1.Size = new Size(e.NewRectangle.Width, e.NewRectangle.Height + 5);
        }

        private void btn1_Click(object sender, EventArgs e)
        {//Metoda rozwijająca(zwijająca) wiersz danych;
            if (!isExpanded) //Jeśli wiersz nie jest rozwinięty, to go rozwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
                lbl2.Visible = false;
                isExpanded = rtbx1.Visible = true;
                this.Height = rtbx1.Height;
            }
            else //Jeśli wiersz jest rozwinięty to go zwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
                lbl2.Visible = true;
                isExpanded = rtbx1.Visible = false;
                this.Height = 39;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {//Metoda otwierająca nowe okno z edytowaniem ilości materiałów; Podajemy potrzebne dane do okienka;
            EditMaterialsView edit = new EditMaterialsView(controller, this, Id, Category, 
                Convert.ToDecimal(Quantity));
            edit.FormClosed += (sen, arg) =>
            {//Po zedytowaniu ilości przypisujemy nową ilość do wiersza danych;
                this.Quantity = Convert.ToString(edit.New_Quantity);
                //Jeśli nowa ilość jest równa 0, to usuwamy z widoku;
                if (Quantity == "0.00" || Quantity == "0,00") this.Dispose();
            };
            edit.Show();
        }
    }
}
