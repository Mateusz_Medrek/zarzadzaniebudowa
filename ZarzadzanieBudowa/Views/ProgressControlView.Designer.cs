﻿namespace ZarzadzanieBudowa.Views
{
    partial class ProgressControlView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl1 = new System.Windows.Forms.Panel();
            this.tlp1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbx1 = new System.Windows.Forms.GroupBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.pnl1.SuspendLayout();
            this.gbx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl1
            // 
            this.pnl1.AutoScroll = true;
            this.pnl1.BackColor = System.Drawing.Color.LightBlue;
            this.pnl1.Controls.Add(this.tlp1);
            this.pnl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl1.Location = new System.Drawing.Point(13, 70);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(1158, 381);
            this.pnl1.TabIndex = 9;
            // 
            // tlp1
            // 
            this.tlp1.AutoSize = true;
            this.tlp1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlp1.ColumnCount = 1;
            this.tlp1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlp1.Location = new System.Drawing.Point(0, 0);
            this.tlp1.Margin = new System.Windows.Forms.Padding(0);
            this.tlp1.MinimumSize = new System.Drawing.Size(1138, 36);
            this.tlp1.Name = "tlp1";
            this.tlp1.RowCount = 2;
            this.tlp1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlp1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlp1.Size = new System.Drawing.Size(1138, 36);
            this.tlp1.TabIndex = 5;
            // 
            // gbx1
            // 
            this.gbx1.Controls.Add(this.lbl1);
            this.gbx1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbx1.Location = new System.Drawing.Point(12, 12);
            this.gbx1.Name = "gbx1";
            this.gbx1.Size = new System.Drawing.Size(900, 55);
            this.gbx1.TabIndex = 10;
            this.gbx1.TabStop = false;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(39, 16);
            this.lbl1.Margin = new System.Windows.Forms.Padding(3);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(270, 32);
            this.lbl1.TabIndex = 3;
            this.lbl1.Text = "Nazwa listy zadań";
            // 
            // ProgressControlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1184, 521);
            this.Controls.Add(this.gbx1);
            this.Controls.Add(this.pnl1);
            this.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Name = "ProgressControlView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kontrola postępu prac";
            this.Load += new System.EventHandler(this.MagazynZamowienia_Load);
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            this.gbx1.ResumeLayout(false);
            this.gbx1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.TableLayoutPanel tlp1;
        private System.Windows.Forms.GroupBox gbx1;
        private System.Windows.Forms.Label lbl1;
    }
}