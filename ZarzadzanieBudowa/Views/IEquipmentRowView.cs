﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Views
{
    public interface IEquipmentRowView
    {
        int Id { get; set; } //Id sprzętu;
        string Category { get; set; } //Kategoria sprzętu;
        string Description { get; set; } //Opis sprzętu;
        bool IsAvailable { get; set; } //Czy sprzęt jest dostępny do wypożyczenia;
        void Dispose(); //Zwolnij zasoby;
    }
}
