﻿namespace ZarzadzanieBudowa.Views
{
    partial class EditBuildingTasksListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ZadaniaPanel = new System.Windows.Forms.Panel();
            this.ZadaniaTable = new System.Windows.Forms.TableLayoutPanel();
            this.PracownicyPanel = new System.Windows.Forms.Panel();
            this.PracownicyTable = new System.Windows.Forms.TableLayoutPanel();
            this.DodajZadanieButton = new System.Windows.Forms.Button();
            this.dateOfListaZadan = new System.Windows.Forms.DateTimePicker();
            this.txtNameOfListaZadan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SzukajTextBox = new System.Windows.Forms.TextBox();
            this.ZapiszButton = new System.Windows.Forms.Button();
            this.ZadaniaPanel.SuspendLayout();
            this.PracownicyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 24);
            this.label1.TabIndex = 22;
            this.label1.Text = "Data realizacji:";
            // 
            // ZadaniaPanel
            // 
            this.ZadaniaPanel.BackColor = System.Drawing.Color.LightBlue;
            this.ZadaniaPanel.Controls.Add(this.ZadaniaTable);
            this.ZadaniaPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZadaniaPanel.Location = new System.Drawing.Point(12, 134);
            this.ZadaniaPanel.Name = "ZadaniaPanel";
            this.ZadaniaPanel.Size = new System.Drawing.Size(684, 374);
            this.ZadaniaPanel.TabIndex = 21;
            // 
            // ZadaniaTable
            // 
            this.ZadaniaTable.AutoSize = true;
            this.ZadaniaTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ZadaniaTable.ColumnCount = 1;
            this.ZadaniaTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ZadaniaTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZadaniaTable.Location = new System.Drawing.Point(0, 0);
            this.ZadaniaTable.Margin = new System.Windows.Forms.Padding(0);
            this.ZadaniaTable.MaximumSize = new System.Drawing.Size(664, 0);
            this.ZadaniaTable.MinimumSize = new System.Drawing.Size(684, 39);
            this.ZadaniaTable.Name = "ZadaniaTable";
            this.ZadaniaTable.RowCount = 2;
            this.ZadaniaTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ZadaniaTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ZadaniaTable.Size = new System.Drawing.Size(684, 39);
            this.ZadaniaTable.TabIndex = 6;
            // 
            // PracownicyPanel
            // 
            this.PracownicyPanel.AutoScroll = true;
            this.PracownicyPanel.BackColor = System.Drawing.Color.LightBlue;
            this.PracownicyPanel.Controls.Add(this.PracownicyTable);
            this.PracownicyPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PracownicyPanel.Location = new System.Drawing.Point(702, 179);
            this.PracownicyPanel.Name = "PracownicyPanel";
            this.PracownicyPanel.Size = new System.Drawing.Size(470, 276);
            this.PracownicyPanel.TabIndex = 28;
            // 
            // PracownicyTable
            // 
            this.PracownicyTable.AutoSize = true;
            this.PracownicyTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PracownicyTable.ColumnCount = 1;
            this.PracownicyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.PracownicyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.PracownicyTable.Location = new System.Drawing.Point(0, 0);
            this.PracownicyTable.Margin = new System.Windows.Forms.Padding(0);
            this.PracownicyTable.MaximumSize = new System.Drawing.Size(450, 0);
            this.PracownicyTable.MinimumSize = new System.Drawing.Size(450, 39);
            this.PracownicyTable.Name = "PracownicyTable";
            this.PracownicyTable.RowCount = 2;
            this.PracownicyTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.PracownicyTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.PracownicyTable.Size = new System.Drawing.Size(450, 39);
            this.PracownicyTable.TabIndex = 1;
            // 
            // DodajZadanieButton
            // 
            this.DodajZadanieButton.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DodajZadanieButton.Location = new System.Drawing.Point(221, 90);
            this.DodajZadanieButton.Name = "DodajZadanieButton";
            this.DodajZadanieButton.Size = new System.Drawing.Size(200, 32);
            this.DodajZadanieButton.TabIndex = 27;
            this.DodajZadanieButton.Text = "DODAJ ZADANIE";
            this.DodajZadanieButton.UseVisualStyleBackColor = true;
            this.DodajZadanieButton.Click += new System.EventHandler(this.DodajZadanieButton_Click);
            // 
            // dateOfListaZadan
            // 
            this.dateOfListaZadan.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateOfListaZadan.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateOfListaZadan.Location = new System.Drawing.Point(221, 6);
            this.dateOfListaZadan.Name = "dateOfListaZadan";
            this.dateOfListaZadan.Size = new System.Drawing.Size(200, 32);
            this.dateOfListaZadan.TabIndex = 26;
            // 
            // txtNameOfListaZadan
            // 
            this.txtNameOfListaZadan.BackColor = System.Drawing.Color.LightBlue;
            this.txtNameOfListaZadan.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtNameOfListaZadan.Location = new System.Drawing.Point(221, 48);
            this.txtNameOfListaZadan.Name = "txtNameOfListaZadan";
            this.txtNameOfListaZadan.Size = new System.Drawing.Size(200, 32);
            this.txtNameOfListaZadan.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(13, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 24);
            this.label3.TabIndex = 24;
            this.label3.Text = "Zadania:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(13, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 23;
            this.label2.Text = "Nazwa:";
            // 
            // SzukajTextBox
            // 
            this.SzukajTextBox.BackColor = System.Drawing.Color.LightBlue;
            this.SzukajTextBox.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SzukajTextBox.Location = new System.Drawing.Point(702, 134);
            this.SzukajTextBox.Name = "SzukajTextBox";
            this.SzukajTextBox.Size = new System.Drawing.Size(457, 39);
            this.SzukajTextBox.TabIndex = 20;
            this.SzukajTextBox.TextChanged += new System.EventHandler(this.SzukajTextBox_TextChanged);
            // 
            // ZapiszButton
            // 
            this.ZapiszButton.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ZapiszButton.Location = new System.Drawing.Point(702, 461);
            this.ZapiszButton.Name = "ZapiszButton";
            this.ZapiszButton.Size = new System.Drawing.Size(470, 47);
            this.ZapiszButton.TabIndex = 29;
            this.ZapiszButton.Text = "ZAPISZ LISTĘ ZADAŃ";
            this.ZapiszButton.UseVisualStyleBackColor = true;
            this.ZapiszButton.Click += new System.EventHandler(this.ZapiszButton_Click);
            // 
            // EditBuildingTasksListView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1184, 521);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ZadaniaPanel);
            this.Controls.Add(this.PracownicyPanel);
            this.Controls.Add(this.DodajZadanieButton);
            this.Controls.Add(this.dateOfListaZadan);
            this.Controls.Add(this.txtNameOfListaZadan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SzukajTextBox);
            this.Controls.Add(this.ZapiszButton);
            this.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Name = "EditBuildingTasksListView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja listy zadań";
            this.Load += new System.EventHandler(this.EdycjaListyZadan_Load);
            this.ZadaniaPanel.ResumeLayout(false);
            this.ZadaniaPanel.PerformLayout();
            this.PracownicyPanel.ResumeLayout(false);
            this.PracownicyPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel ZadaniaPanel;
        private System.Windows.Forms.Panel PracownicyPanel;
        private System.Windows.Forms.Button DodajZadanieButton;
        private System.Windows.Forms.DateTimePicker dateOfListaZadan;
        private System.Windows.Forms.TextBox txtNameOfListaZadan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SzukajTextBox;
        private System.Windows.Forms.Button ZapiszButton;
        private System.Windows.Forms.TableLayoutPanel PracownicyTable;
        private System.Windows.Forms.TableLayoutPanel ZadaniaTable;
    }
}