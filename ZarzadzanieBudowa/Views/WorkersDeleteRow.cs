﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersDeleteRow : UserControl, IWorkersRowView
    {
        private WorkerController controller; //Kontroler dla widoku;

        public WorkersDeleteRow(WorkerController controller)
        {
            InitializeComponent();
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public int Id { get; set; } //Id pracownika;

        public string WorkerName //Imię pracownika;
        {
            get { return imie.Text; }
            set { imie.Text = value; }
        }

        public string WorkerSurname //Nazwisko pracownika;
        {
            get { return nazwisko.Text; }
            set { nazwisko.Text = value; }
        }

        public string Pesel //Pesel pracownika;
        {
            get { return pesel.Text; }
            set { pesel.Text = value; }
        }

        public string EmploymentDate //Data zatrudnienia pracownika;
        {
            get { return dataZatrud.Text; }
            set { dataZatrud.Text = value; }
        }

        public string Status //Status pracownika;
        {
            get { return status.Text; }
            set { status.Text = value; }
        }

        private void WorkersDeleteRow_DoubleClick(object sender, EventArgs e)
        {
            //Zdarzenie dwukrotnego kliknięcia;
            if (MessageBox.Show("Czy na pewno chcesz usunąć dane?", "",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int tmp = -1;
                tmp = controller.RemoveWorker(this);
                if (tmp >= 1) //Jeśli operacja się udała, zagraj dźwięk, pokaż komunikat;
                {
                    SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Dane usunięto");
                    this.Dispose(); //Usuń TEN rekord;
                }
                else MessageBox.Show("Coś poszło nie tak");
                
            }
        }
    }
}
