﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class EquipmentRow : UserControl, IEquipmentRowView
    {
        private bool isExpanded; //Czy wiersz danych jest rozwinięty;
        private bool isAvailable; //Czy jest sprzęt jest dostępny do wypożyczenia;
        private EquipmentController controller; //Kontroler dla widoku;

        public EquipmentRow(EquipmentController controller)
        {
            InitializeComponent();
            isExpanded = false; //Domyślnie wiersz ma być zwinięty;
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        public int Id { get; set; } //Id materiałów;

        public string Category //Kategoria materiałów;
        {
            get { return lbl1.Text; }
            set { lbl1.Text = value; }
        }

        private string ShortDescription //Opis gdy wiersz danych jest zwinięty;
        {
            get { return lbl2.Text; }
            set
            {
                if (value.Length <= 16) //Jeśli długość kategorii większa niż 16,
                    lbl2.Text = value;  //to przypisz tylko pierwsze 13 znaków oraz wielokropek;
                else
                    lbl2.Text = value.Substring(0, 13) + "...";
            }
        }

        public string Description //Opis gdy wiersz danych jest rozwinięty;
        {
            get { return rtbx1.Text; }
            set
            {
                rtbx1.Text = value;
                ShortDescription = value;
            }
        }

        public bool IsAvailable //Czy jest sprzęt jest dostępny do wypożyczenia;
        {
            get { return isAvailable; }
            set
            {
                isAvailable = value;
                if (value == true) btn2.Text = "Wydaj sprzęt";
                else if (value == false) btn2.Text = "Zwróć sprzęt";
            }
        }

        private void rtbx1_ContentsResized(object sender, ContentsResizedEventArgs e)
        {//Rozszerzanie textboxa tak aby dopasowywał się do swojej zawartości;
            rtbx1.Size = new Size(e.NewRectangle.Width, e.NewRectangle.Height + 5);
        }

        private void btn1_Click(object sender, EventArgs e)
        {//Metoda rozwijająca(zwijająca) wiersz danych;
            if (!isExpanded) //Jeśli wiersz nie jest rozwinięty, to go rozwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowUp);
                lbl2.Visible = false;
                isExpanded = rtbx1.Visible = true;
                this.Height = rtbx1.Height;
            }
            else //Jeśli wiersz jest rozwinięty to go zwiń;
            {
                btn1.BackgroundImage = new Bitmap(Properties.Resources.ArrowDown);
                lbl2.Visible = true;
                isExpanded = rtbx1.Visible = false;
                this.Height = 39;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz zmienić status sprzętu?", "",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {//Jeśli użytkownik potwierdzi chęć wykonania operacji to wykonujemy uaktualniamy model i BD;
                //Uaktualnij widok;
                if (IsAvailable == true) IsAvailable = false; //Zmiana statusu;
                else IsAvailable = true;
                int tmp = controller.UpdateModelValues(this); //Uaktualnij model i BD;
                if (tmp >= 1)
                {
                    SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Zmieniono status");
                }
                else //Jeśli coś poszło nie tak, to zmieniamy status na poprzedni;
                {
                    if (IsAvailable == true) IsAvailable = false;
                    else IsAvailable = true;
                    MessageBox.Show("Coś poszło nie tak.");
                }
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć sprzęt?", "",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {//Jeśli użytkownik potwierdzi chęć wykonania operacji to usuwamy z modelu, widoku i DB;
                int tmp = -1;
                tmp = controller.RemoveEquipment(this);
                if (tmp >= 1)
                {
                    SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Usunięto");
                    this.Dispose(); //Usunąć rekord;
                }
                else MessageBox.Show("Coś poszło nie tak");
            }
        }
    }
}
