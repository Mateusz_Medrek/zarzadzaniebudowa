﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class ReportView : Form, IReportView
    {
        private ReportController controller; //Kontroler dla widoku;

        public string Author //Autor raportu;
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }

        public string Content //Treść raportu;
        {
            get { return richTextBox1.Text; }
            set { richTextBox1.Text = value; }
        }

        public DateTime CreateDate //Data utworzenia raportu;
        {
            get { return dateTimePicker1.Value; }
            set { dateTimePicker1.Value = value; }
        }

        public string ReportName //Nazwa raportu;
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        public ReportView(string login)
        {
            InitializeComponent();
            textBox2.Text = login; //Ustaw użytkownika jako autora raportu;
        }

        public void SetController(ReportController controller) //Ustaw kontroler dla widoku;
        {
            this.controller = controller;
        }

        private void Raporty_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MinDate = Convert.ToDateTime("01-01-1970"); //Ustaw minimalną datę;
            dateTimePicker1.MaxDate = DateTime.Now; //Ustaw maksymalną datę;
            dateTimePicker1.Value = dateTimePicker1.MaxDate; //Ustaw obecnie wybraną wartość;
            toolTip1.SetToolTip(btn1, "Skrót: Ctrl + D"); //Wyświetla podpowiedz do przycisku
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            string pattern = "[A-Za-ząęćśłóźżńŻŁŚĄĘŹ0-9_]";
            Match match = Regex.Match(ReportName, pattern);

            if (ReportName == "") //Jeśli nazwa raportu jest pusta;
            {
                MessageBox.Show("Nazwa raportu nie może być pusta!");
                return;
            }
            else if (!match.Success)
            {
                MessageBox.Show("Nazwa raportu zawiera niepoprawne znaki!");
                return;
            }

            if (Content == "") //Jeśli treść raportu jest pusta;
            {
                MessageBox.Show("Treść raportu nie może być pusta!");
                return;
            }

            match = Regex.Match(Author, pattern);
            if (Author == "") //Jeśli autor raportu jest pusty;
            {
                MessageBox.Show("Podaj Autora!");
                return;
            }
            else if (!match.Success)
            {
                MessageBox.Show("Imię/Nazwisko autora zawiera niepoprawne znaki!");
                return;
            }

            if (MessageBox.Show("Czy chcesz dodać raport?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int tmp = -1;
                tmp = controller.UpdateModelValues(); //Kontroler aktualizuje model i BD;
                if (tmp >= 1) //Jeśli operacja się udała, zagraj dźwięk, pokaż komunikat i zamknij TO okno;
                {
                    SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                    zwyciestwo.Play();
                    MessageBox.Show("Dodano!", "", MessageBoxButtons.OK);
                    this.Close();
                }
                else MessageBox.Show("Coś poszło nie tak");
            }
        }

        //Skrót Klawiszowy Control + D
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D && e.Modifiers == Keys.Control)
            {
                btn1_Click(this, EventArgs.Empty);
            }
        }

        //Skrót Klawiszowy Control + D
        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D && e.Modifiers == Keys.Control)
            {
                btn1_Click(this, EventArgs.Empty);
            }
        }
    }
}
