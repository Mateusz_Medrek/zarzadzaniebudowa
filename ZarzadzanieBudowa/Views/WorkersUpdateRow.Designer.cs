﻿namespace ZarzadzanieBudowa.Views
{
    partial class WorkersUpdateRow
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.status = new System.Windows.Forms.Label();
            this.dataZatrud = new System.Windows.Forms.Label();
            this.pesel = new System.Windows.Forms.Label();
            this.imie = new System.Windows.Forms.Label();
            this.nazwisko = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.BackColor = System.Drawing.Color.LightBlue;
            this.status.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.status.Location = new System.Drawing.Point(899, 3);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(165, 32);
            this.status.TabIndex = 9;
            this.status.Text = "0123456789";
            this.status.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            // 
            // dataZatrud
            // 
            this.dataZatrud.AutoSize = true;
            this.dataZatrud.BackColor = System.Drawing.Color.LightBlue;
            this.dataZatrud.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataZatrud.Location = new System.Drawing.Point(728, 3);
            this.dataZatrud.Name = "dataZatrud";
            this.dataZatrud.Size = new System.Drawing.Size(165, 32);
            this.dataZatrud.TabIndex = 8;
            this.dataZatrud.Text = "0123456789";
            this.dataZatrud.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            // 
            // pesel
            // 
            this.pesel.AutoSize = true;
            this.pesel.BackColor = System.Drawing.Color.LightBlue;
            this.pesel.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pesel.Location = new System.Drawing.Point(542, 3);
            this.pesel.Name = "pesel";
            this.pesel.Size = new System.Drawing.Size(180, 32);
            this.pesel.TabIndex = 7;
            this.pesel.Text = "01234567890";
            this.pesel.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            // 
            // imie
            // 
            this.imie.AutoSize = true;
            this.imie.BackColor = System.Drawing.Color.LightBlue;
            this.imie.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.imie.Location = new System.Drawing.Point(20, 3);
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(255, 32);
            this.imie.TabIndex = 6;
            this.imie.Text = "0123456789012345";
            this.imie.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            // 
            // nazwisko
            // 
            this.nazwisko.AutoSize = true;
            this.nazwisko.BackColor = System.Drawing.Color.LightBlue;
            this.nazwisko.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwisko.Location = new System.Drawing.Point(281, 3);
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(255, 32);
            this.nazwisko.TabIndex = 5;
            this.nazwisko.Text = "0123456789012345";
            this.nazwisko.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            // 
            // WorkersUpdateRow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.Controls.Add(this.status);
            this.Controls.Add(this.dataZatrud);
            this.Controls.Add(this.pesel);
            this.Controls.Add(this.imie);
            this.Controls.Add(this.nazwisko);
            this.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "WorkersUpdateRow";
            this.Size = new System.Drawing.Size(1100, 39);
            this.DoubleClick += new System.EventHandler(this.WorkersUpdateRow_DoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Label dataZatrud;
        private System.Windows.Forms.Label pesel;
        private System.Windows.Forms.Label imie;
        private System.Windows.Forms.Label nazwisko;
    }
}
