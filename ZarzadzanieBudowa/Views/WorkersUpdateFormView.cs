﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersUpdateFormView : Form, INewEditWorkersView
    {
        private NewEditWorkerController controller; //Kontroler dla widoku;

        public WorkersUpdateFormView(int id, 
            string imie, string nazwisko, string pesel, DateTime data, int statusId)
        {
            InitializeComponent();
            Id = id;
            WorkerName = imie;
            WorkerSurname = nazwisko;
            Pesel = pesel;
            EmploymentDate = data;
            CategoriesSelectedValue = statusId;
        }

        public int Id { get; set; } //Id modyfikowanego pracownika;

        public string WorkerName //Imię modyfikowanego pracownika;
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public string WorkerSurname //Nazwisko modyfikowanego pracownika;
        {
            get { return txtSurname.Text; } 
            set { txtSurname.Text = value; }
        }

        public string Pesel //Pesel modyfikowanego pracownika;
        {
            get { return txtPsl.Text; }
            set { txtPsl.Text = value; }
        }

        public DateTime EmploymentDate //Data zatrudnienia modyfikowanego pracownika;
        {
            get { return datePicker1.Value; }
            set { datePicker1.Value = value; }
        }

        public string Status //Status modyfikowanego pracownika;
        {
            get { return (string)(cmbBox1.DataSource as DataTable).Rows[(int)cmbBox1.SelectedValue - 1][1]; }
        }

        public DataTable Categories //Źródło danych ComboBoxa ze statusami;
        {
            get { return cmbBox1.DataSource as DataTable; }
            set { cmbBox1.DataSource = value; }
        }

        public string CategoriesDisplayMember //Dane, które są wyświetlane użytkownikowi w ComboBoxie;
        {
            get { return cmbBox1.DisplayMember; }
            set { cmbBox1.DisplayMember = value; }
        }
        public string CategoriesValueMember //Dane, które są przekazywane jako wartość przez ComboBoxa;
        {
            get { return cmbBox1.ValueMember; }
            set { cmbBox1.ValueMember = value; }
        }

        public object CategoriesSelectedValue //Obecnie wybrana wartość ComboBoxa;
        {
            get { return cmbBox1.SelectedValue; }
            set { cmbBox1.SelectedValue = value; }
        }

        public void SetController(NewEditWorkerController controller)
        {
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Jeśli nie podano treści, nie zapisuj;
            if (txtName.Text == String.Empty || txtSurname.Text == String.Empty || txtPsl.Text == String.Empty)
                MessageBox.Show("Podaj wszystkie dane!");
            else //Sprawdź czy podano poprawne dane, jeśli nie to nie zapisuj;
            {
                string pattern = "[A-Za-ząęćśłóźżńŻŁŚĄĘŹ]"; //Wyrażenie regularne dla imienia i nazwiska;
                Match match = Regex.Match(WorkerName, pattern);

                if (!match.Success)
                {
                    MessageBox.Show("Imię zawiera niepoprawne znaki!");
                    return;
                }

                match = Regex.Match(WorkerSurname, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("Nazwisko zawiera niepoprawne znaki!");
                    return;
                }

                pattern = @"^\d{11}$"; //Wyrażenie regularne dla pesela;
                match = Regex.Match(Pesel, pattern);
                if (!match.Success)
                {
                    MessageBox.Show("PESEL zawiera niepoprawne znaki!");
                    return;
                }

                //Zapisz dane do BD;
                if (MessageBox.Show("Czy na pewno chcesz zmodyfikować dane?", "",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int tmp = -1;
                    tmp = controller.UpdateInDB(this);
                    if (tmp >= 1) //Jeśli operacja się udała, zagraj dźwięk, pokaż komunikat i zamknij TO okno;
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Dane zmodyfikowano");
                        this.Close();
                    }
                    else MessageBox.Show("Coś poszło nie tak");
                }
            }
            
        }
    }
}
