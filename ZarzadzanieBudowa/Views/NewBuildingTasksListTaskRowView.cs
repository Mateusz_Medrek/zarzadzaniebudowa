﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewBuildingTasksListTaskRowView : UserControl
    {
        public NewBuildingTasksListTaskRowView()
        {
            InitializeComponent();
            isExpanded = true;
            show();
        }
        public bool isExpanded; //Czy zadanie jest zwinięte;
        public int Row_Id { get; set; } //Id zadania;
        public string Tytul //Tytul zadania;
        {
            get { return tytulZadaniaTextBox.Text; }
            set { tytulZadaniaTextBox.Text = LabelTytulZadania.Text = value; }
        }
        public string Tresc //Tresc zadania;
        {
            get { return trescZadaniaTextBox.Text; }
            set { trescZadaniaTextBox.Text = value; }
        }
        public ControlCollection Pracownicy //Kolekcja przydzielonych pracowników;
        {
            get { return PrzydzieleniPracownicyFlowLayoutPanel.Controls; }
        }

        private void UsunZadanie_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void show()
        {
            if(isExpanded == true) //Rozwiń zadanie;
            {
                label1.Visible = label2.Visible = label3.Visible = label4.Visible = true;
                LabelTytulZadania.Visible = label5.Visible = false;
                trescZadaniaTextBox.Visible = tytulZadaniaTextBox.Visible = PrzydzieleniPracownicyFlowLayoutPanel.Visible = true;
            }
            else //Zwiń zadanie;
            {
                this.Height = 39;
                LabelTytulZadania.Visible = label5.Visible =true;
                label4.Visible = false;
                label2.Visible = label3.Visible = false;
                trescZadaniaTextBox.Visible = tytulZadaniaTextBox.Visible = PrzydzieleniPracownicyFlowLayoutPanel.Visible = false;
                UsunZadanie.Visible = false;
            }
        }

        private void ZadanieNowaListaZadanRow_DragDrop(object sender, DragEventArgs e)
        {
            bool czy_dodany = false;
            AssignedWorkerRowView pracownik = new AssignedWorkerRowView();
            pracownik.Row_Id = Convert.ToInt16(e.Data.GetData(DataFormats.Text).ToString());
            var row = DBConnection.SelectQuery("SELECT pracownik_imie, pracownik_nazwisko, pracownik_status_id " +
                "FROM pracownicy WHERE pracownik_id = " + pracownik.Row_Id).Tables[0].Rows[0];
            pracownik.Dane = row.Field<string>("pracownik_imie") + ' ' + row.Field<string>("pracownik_nazwisko");
            foreach(AssignedWorkerRowView p in Pracownicy)
            {
                if(p.Row_Id == pracownik.Row_Id)
                {
                    czy_dodany = true;
                }
            }
            if (czy_dodany == false)
            {
                if ((row.Field<int>("pracownik_status_id")) == 1)
                { 
                Pracownicy.Add(pracownik);
                }
                else
                {
                    if (MessageBox.Show("Wybrany pracownik jest obecnie niedostępny. " +
                        "Czy nadal chcesz przydzielić go do tego zadania?", "", 
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Pracownicy.Add(pracownik);
                    }
                }
            }
            else
            {
                MessageBox.Show("Ten pracownik jest już dodany do tego zadania");
            }
        }

        private void ZadanieNowaListaZadanRow_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void ZadanieNowaListaZadanRow_MouseClick(object sender, MouseEventArgs e)
        {
            if(isExpanded == true && !string.IsNullOrEmpty(tytulZadaniaTextBox.Text) 
                && !string.IsNullOrEmpty(trescZadaniaTextBox.Text) && Pracownicy.Count > 0)
            {
                this.isExpanded = false;
            }
            else
            {
                this.isExpanded = true;
            }
            this.show();
        }

        private void LabelTytulZadania_Click(object sender, EventArgs e)
        {
            if (isExpanded == true && !string.IsNullOrEmpty(Tytul) && !string.IsNullOrEmpty(Tresc) 
                && Pracownicy.Count > 0)
            {
                this.isExpanded = false;
            }
            else
            {
                this.isExpanded = true;
            }
            this.show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (isExpanded == true && !string.IsNullOrEmpty(Tytul) && !string.IsNullOrEmpty(Tresc) 
                && Pracownicy.Count > 0)
            {
                this.isExpanded = false;
            }
            else
            {
                this.isExpanded = true;
            }
            this.show();
        }
    }
}
