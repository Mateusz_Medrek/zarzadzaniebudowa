﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public interface IReportView
    {
        string Author { get; set; }
        string Content { get; set; }
        DateTime CreateDate { get; set; }
        string ReportName { get; set; }
        void SetController(ReportController controller);
    }
}
