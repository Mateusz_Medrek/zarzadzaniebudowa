﻿namespace ZarzadzanieBudowa.Views
{
    partial class NewBuildingTasksListTaskRowView
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.tytulZadaniaTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trescZadaniaTextBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UsunZadanie = new System.Windows.Forms.Button();
            this.PrzydzieleniPracownicyFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelTytulZadania = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tytulZadaniaTextBox
            // 
            this.tytulZadaniaTextBox.Font = new System.Drawing.Font("Consolas", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tytulZadaniaTextBox.Location = new System.Drawing.Point(227, 3);
            this.tytulZadaniaTextBox.Name = "tytulZadaniaTextBox";
            this.tytulZadaniaTextBox.Size = new System.Drawing.Size(267, 33);
            this.tytulZadaniaTextBox.TabIndex = 3;
            this.tytulZadaniaTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 26);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tytuł zadania:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 26);
            this.label2.TabIndex = 8;
            this.label2.Text = "Treść zadania:";
            // 
            // trescZadaniaTextBox
            // 
            this.trescZadaniaTextBox.BackColor = System.Drawing.Color.Azure;
            this.trescZadaniaTextBox.Font = new System.Drawing.Font("Consolas", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.trescZadaniaTextBox.Location = new System.Drawing.Point(227, 57);
            this.trescZadaniaTextBox.Name = "trescZadaniaTextBox";
            this.trescZadaniaTextBox.Size = new System.Drawing.Size(418, 110);
            this.trescZadaniaTextBox.TabIndex = 9;
            this.trescZadaniaTextBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(300, 26);
            this.label3.TabIndex = 11;
            this.label3.Text = "Przydzieleni pracownicy:";
            // 
            // UsunZadanie
            // 
            this.UsunZadanie.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UsunZadanie.Location = new System.Drawing.Point(432, 313);
            this.UsunZadanie.Name = "UsunZadanie";
            this.UsunZadanie.Size = new System.Drawing.Size(213, 41);
            this.UsunZadanie.TabIndex = 13;
            this.UsunZadanie.Text = "USUŃ ZADANIE";
            this.UsunZadanie.UseVisualStyleBackColor = true;
            this.UsunZadanie.Click += new System.EventHandler(this.UsunZadanie_Click);
            // 
            // PrzydzieleniPracownicyFlowLayoutPanel
            // 
            this.PrzydzieleniPracownicyFlowLayoutPanel.AutoScroll = true;
            this.PrzydzieleniPracownicyFlowLayoutPanel.Location = new System.Drawing.Point(9, 218);
            this.PrzydzieleniPracownicyFlowLayoutPanel.MaximumSize = new System.Drawing.Size(639, 0);
            this.PrzydzieleniPracownicyFlowLayoutPanel.MinimumSize = new System.Drawing.Size(639, 59);
            this.PrzydzieleniPracownicyFlowLayoutPanel.Name = "PrzydzieleniPracownicyFlowLayoutPanel";
            this.PrzydzieleniPracownicyFlowLayoutPanel.Size = new System.Drawing.Size(639, 89);
            this.PrzydzieleniPracownicyFlowLayoutPanel.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.InfoText;
            this.label5.Location = new System.Drawing.Point(0, 39);
            this.label5.MaximumSize = new System.Drawing.Size(664, 2);
            this.label5.MinimumSize = new System.Drawing.Size(684, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(684, 2);
            this.label5.TabIndex = 16;
            // 
            // LabelTytulZadania
            // 
            this.LabelTytulZadania.AutoSize = true;
            this.LabelTytulZadania.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelTytulZadania.Location = new System.Drawing.Point(221, 3);
            this.LabelTytulZadania.MaximumSize = new System.Drawing.Size(400, 33);
            this.LabelTytulZadania.MinimumSize = new System.Drawing.Size(300, 33);
            this.LabelTytulZadania.Name = "LabelTytulZadania";
            this.LabelTytulZadania.Size = new System.Drawing.Size(300, 33);
            this.LabelTytulZadania.TabIndex = 7;
            this.LabelTytulZadania.Text = "Tytuł zadania:";
            this.LabelTytulZadania.Click += new System.EventHandler(this.LabelTytulZadania_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.InfoText;
            this.label4.Location = new System.Drawing.Point(0, 365);
            this.label4.MaximumSize = new System.Drawing.Size(664, 2);
            this.label4.MinimumSize = new System.Drawing.Size(684, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(684, 2);
            this.label4.TabIndex = 17;
            // 
            // NewBuildingTasksListTaskRowView
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PrzydzieleniPracownicyFlowLayoutPanel);
            this.Controls.Add(this.UsunZadanie);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.trescZadaniaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelTytulZadania);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tytulZadaniaTextBox);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(664, 0);
            this.MinimumSize = new System.Drawing.Size(684, 39);
            this.Name = "NewBuildingTasksListTaskRowView";
            this.Size = new System.Drawing.Size(684, 367);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.ZadanieNowaListaZadanRow_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.ZadanieNowaListaZadanRow_DragEnter);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ZadanieNowaListaZadanRow_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox tytulZadaniaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox trescZadaniaTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button UsunZadanie;
        private System.Windows.Forms.FlowLayoutPanel PrzydzieleniPracownicyFlowLayoutPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelTytulZadania;
        private System.Windows.Forms.Label label4;
    }
}
