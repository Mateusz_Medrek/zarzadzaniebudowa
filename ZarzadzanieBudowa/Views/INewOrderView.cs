﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public interface INewOrderView
    {
        int Id { get; set; } //Id zamówienia w BD;
        string Description { get; set; } //Opis nowego zamówienia;
        void SetController(NewOrderController controller); //Ustaw kontroler dla widoku;
    }
}
