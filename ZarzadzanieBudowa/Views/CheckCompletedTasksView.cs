﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class CheckCompletedTasksView : Form
    {
        private int id; //Id listy zadań;
        private string Nazwa; //Nazwa listy zadań;
        private string Realizacji; //Data realizacji listy zadań;
        private string Utworzenia; //Data utworzenia listy zadań;
        private string OstatniejModyfikacji; //Data ostatniej modyfikacji listy zadań;

        public CheckCompletedTasksView(int id, string Nazwa, string Realizacji, string Utworzenia, 
            string OstatnieModyfikacji)
        {
            InitializeComponent();
            this.Nazwa = Nazwa;
            this.id = id;
            this.Realizacji = Realizacji;
            this.Utworzenia = Utworzenia;
            this.OstatniejModyfikacji = OstatnieModyfikacji;
            lbl2.Text = Nazwa;
            lbl1.Text = Utworzenia;
            label2.Text = OstatniejModyfikacji;
            label3.Text = Realizacji;
        }
        private void ShowZadania()
        {
            #region Filling our panel with data
            //Wyszukaj zadania dla danej listy zadań;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM zadanie where lista_zadan_id = @id",
                new DBParameter("@id", id));
            //Wyczyść kontener;
            tlp1.Controls.Clear();
            tlp1.RowCount = 2;
            //Ustaw co się dzieje po zajściu poniższych zdarzeń;
            tlp1.ControlAdded += (s, args) => tlp1.RowCount++;
            tlp1.ControlRemoved += (s, args) => tlp1.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego zadania;
            {
                //Dodaj do zadanie do kontenera;
                tlp1.Controls.Add(new BuildingTaskView
                {
                    Row_Id = row.Field<int>("zadanie_id"),
                    Nazwa = row.Field<string>("zadanie_tytul"),
                    Tresc = row.Field<string>("zadanie_tresc"),
                    CzySkonczone = row.Field<bool>("Czy_Skonczone")
                }, 0, tlp1.RowCount - 1);
            }
            #endregion
        }

        private void MagazynZamowienia_Load(object sender, EventArgs e)
        {
            ShowZadania();
        }
    }
}
