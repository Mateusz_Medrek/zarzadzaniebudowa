﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class NewOrderView : Form, INewOrderView
    {
        private NewOrderController controller; //Kontroler dla widoku;

        public int Id { get; set; } //Id zamówienia w BD;

        public string Description //Opis zamówienia;
        {
            get { return rtbx1.Text; }
            set { rtbx1.Text = value; }
        }

        public NewOrderView() //Konstruktor wywoływany gdy dodajemy zamówienie;
        {
            InitializeComponent();
        }

        public NewOrderView(int id, string tresc) //Konstruktor wywoływany gdy edytujemy zamówienie;
        {
            InitializeComponent();
            Id = id;
            Description = tresc;
            btn1.Text = "Edytuj";
        }

        public void SetController(NewOrderController controller) //Ustaw kontroler dla widoku;
        {
            this.controller = controller;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //Jeśli nie podano treści, nie zapisuj;
            if (rtbx1.Text == String.Empty) MessageBox.Show("Podaj treść zamówienia!");
            else
            {
                if (MessageBox.Show("Czy chcesz zapisać zmiany?", "",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {//Jeśli użytkownik potwierdzi chęć zapisania w BD;
                    int tmp = -1;
                    if (btn1.Text == "Edytuj") //Sprawdź czy wstawić nowe zamówienie, czy edytować istniejące;
                    {
                        //Kontroler edytuje zamówienie w modelu, BD i widoku;
                        tmp = controller.EditInDB();
                    }
                    else
                    {
                        //Uzyskaj obecną datę - użyj jej jako data utworzenia zamówienia;
                        DateTime dt = DateTime.Now;
                        //Kontroler dodaje zamówienie do modelu, BD i widoku;
                        tmp = controller.AddToDB();
                    }
                    if (tmp >= 1)
                    {
                        SoundPlayer zwyciestwo = new SoundPlayer(Properties.Resources.WIN);
                        zwyciestwo.Play();
                        MessageBox.Show("Zapisano");
                        this.Close(); //Zamknij to okno;
                    }
                    else MessageBox.Show("Coś poszło nie tak");
                }
            }
        }
    }
}
