﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public partial class WorkersUpdateRow : UserControl, IWorkersRowView
    {
        private WorkerController controller; //Kontroler dla widoku;

        public int Id { get; set; } //Id pracownika;

        public string WorkerName //Imię pracownika;
        {
            get { return imie.Text; }
            set { imie.Text = value; }
        }

        public string WorkerSurname //Nazwisko pracownika;
        {
            get { return nazwisko.Text; }
            set { nazwisko.Text = value; }
        }

        public string Pesel //Pesel pracownika;
        {
            get { return pesel.Text; }
            set { pesel.Text = value; }
        }

        public string EmploymentDate //Data zatrudnienia pracownika;
        {
            get { return dataZatrud.Text; }
            set { dataZatrud.Text = value; }
        }

        public int Status_Id { get; set; } //Id statusu pracownika;

        public string Status //Status pracownika;
        {
            get { return status.Text; }
            set { status.Text = value; }
        }

        public WorkersUpdateRow(WorkerController controller)
        {
            InitializeComponent();
            this.controller = controller; //Ustaw kontroler dla widoku;
        }

        private void WorkersUpdateRow_DoubleClick(object sender, EventArgs e)
        {
            //Zdarzenie dwukrotnego kliknięcia;
            //Stwórz nowe okno z formularzem do modyfikowania pracownika (przekaż obecne dane);
            WorkersUpdateFormView workersUpdateForm = new WorkersUpdateFormView(Id, WorkerName, 
                WorkerSurname, Pesel, Convert.ToDateTime(EmploymentDate), Status_Id);
            //Załaduj dane do ComboBoxa;
            controller.LoadComboBox(workersUpdateForm);
            if (controller.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            //Stwórz kontroler dla nowego okna i przypisz mu nowe okno;
            NewEditWorkerController updateController = new NewEditWorkerController(workersUpdateForm);
            if (updateController.DbDead)
            {
                MessageBox.Show("Coś poszło nie tak");
                return;
            }
            //Zdarzenie określające co sięstanie po zamknięciu nowego okna;
            workersUpdateForm.FormClosed += (s, args) => 
            {
                //Przypisanie zmodyfikowanych danych;
                WorkerName = workersUpdateForm.WorkerName;
                WorkerSurname = workersUpdateForm.WorkerSurname;
                Pesel = workersUpdateForm.Pesel;
                EmploymentDate = workersUpdateForm.EmploymentDate.ToShortDateString();
                Status_Id = (int)workersUpdateForm.CategoriesSelectedValue;
                Status = workersUpdateForm.Status;
            };
            //Pokaż nowe okno;
            workersUpdateForm.Show();
        }
    }
}
