﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class LoadView : Form
    {
        private string Nazwa;
        private int id;
        public LoadView(int id, string Nazwa)
        {
            InitializeComponent();
            this.id = id;
            this.Nazwa = Nazwa;
        }
        private void ShowPracownicy()
        {
            //Wyszukaj nieukończone zadania, które są przydzielone danemu pracownikowi;
            DataSet ds = DBConnection.SelectQuery("SELECT zadanie_id,lista_zadan_nazwa,zadanie_tytul,Czy_Skonczone FROM zadanie_pracownicy INNER JOIN zadanie USING (zadanie_id) INNER JOIN listy_zadan USING (lista_zadan_id) WHERE  pracownik_id = @id AND Czy_Skonczone = 0",
                new DBParameter("@id", id));
            //Wyczyść kontener;
            tableLayoutPanel2.Controls.Clear();
            tableLayoutPanel2.RowCount = 2;
            //Ustal co siędzieje po zajściu poniższych zdarzeń;
            tableLayoutPanel2.ControlAdded += (s, args) => tableLayoutPanel2.RowCount++;
            tableLayoutPanel2.ControlRemoved += (s, args) => tableLayoutPanel2.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego zadania;
            {
                //Dodaj zadanie do kontenera;
                tableLayoutPanel2.Controls.Add(new BuildingTaskTitleView
                {
                    Row_Id = row.Field<int>("zadanie_id"),
                    Nazwa = row.Field<string>("zadanie_tytul"),
                    NazwaListy = row.Field<string>("lista_zadan_nazwa")
                }, 0, tableLayoutPanel2.RowCount - 1);
            }
        }
       
        private void NowaListaZadan_Load(object sender, EventArgs e)
        {
            ShowPracownicy();
        }
    }
}
