﻿namespace ZarzadzanieBudowa.Views
{
    partial class WorkersCreateView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.tbx1 = new System.Windows.Forms.TextBox();
            this.tbx2 = new System.Windows.Forms.TextBox();
            this.tbx3 = new System.Windows.Forms.TextBox();
            this.cmbbx1 = new System.Windows.Forms.ComboBox();
            this.lbl4 = new System.Windows.Forms.Label();
            this.datePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lbl5 = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(12, 13);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(330, 32);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Podaj IMIĘ pracownika";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl2.Location = new System.Drawing.Point(12, 57);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(390, 32);
            this.lbl2.TabIndex = 1;
            this.lbl2.Text = "Podaj NAZWISKO pracownika";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl3.Location = new System.Drawing.Point(12, 101);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(345, 32);
            this.lbl3.TabIndex = 2;
            this.lbl3.Text = "Podaj PESEL pracownika";
            // 
            // tbx1
            // 
            this.tbx1.BackColor = System.Drawing.Color.LightBlue;
            this.tbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx1.Location = new System.Drawing.Point(408, 12);
            this.tbx1.Name = "tbx1";
            this.tbx1.Size = new System.Drawing.Size(300, 39);
            this.tbx1.TabIndex = 3;
            // 
            // tbx2
            // 
            this.tbx2.BackColor = System.Drawing.Color.LightBlue;
            this.tbx2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx2.Location = new System.Drawing.Point(408, 57);
            this.tbx2.Name = "tbx2";
            this.tbx2.Size = new System.Drawing.Size(300, 39);
            this.tbx2.TabIndex = 4;
            // 
            // tbx3
            // 
            this.tbx3.BackColor = System.Drawing.Color.LightBlue;
            this.tbx3.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx3.Location = new System.Drawing.Point(408, 102);
            this.tbx3.Name = "tbx3";
            this.tbx3.Size = new System.Drawing.Size(300, 39);
            this.tbx3.TabIndex = 5;
            // 
            // cmbbx1
            // 
            this.cmbbx1.BackColor = System.Drawing.Color.LightBlue;
            this.cmbbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cmbbx1.FormattingEnabled = true;
            this.cmbbx1.Items.AddRange(new object[] {
            "Dostępny",
            "ELQUATRO",
            "Urlop",
            "Szkolenie"});
            this.cmbbx1.Location = new System.Drawing.Point(408, 146);
            this.cmbbx1.Name = "cmbbx1";
            this.cmbbx1.Size = new System.Drawing.Size(300, 40);
            this.cmbbx1.TabIndex = 6;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl4.Location = new System.Drawing.Point(12, 146);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(390, 32);
            this.lbl4.TabIndex = 7;
            this.lbl4.Text = "Wybierz STATUS pracownika";
            // 
            // datePicker1
            // 
            this.datePicker1.CalendarFont = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.datePicker1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.datePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker1.Location = new System.Drawing.Point(408, 192);
            this.datePicker1.Name = "datePicker1";
            this.datePicker1.Size = new System.Drawing.Size(300, 39);
            this.datePicker1.TabIndex = 8;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl5.Location = new System.Drawing.Point(12, 192);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(360, 64);
            this.lbl5.TabIndex = 9;
            this.lbl5.Text = "Wybierz DATĘ \r\nZATRUDNIENIA pracownika";
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(408, 274);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(300, 50);
            this.btn1.TabIndex = 10;
            this.btn1.Text = "Dodaj pracownika";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // WorkersCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(734, 336);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.datePicker1);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.cmbbx1);
            this.Controls.Add(this.tbx3);
            this.Controls.Add(this.tbx2);
            this.Controls.Add(this.tbx1);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinimumSize = new System.Drawing.Size(750, 375);
            this.Name = "WorkersCreate";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodawanie pracownika";
            this.Load += new System.EventHandler(this.WorkersCreate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.TextBox tbx1;
        private System.Windows.Forms.TextBox tbx2;
        private System.Windows.Forms.TextBox tbx3;
        private System.Windows.Forms.ComboBox cmbbx1;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.DateTimePicker datePicker1;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}