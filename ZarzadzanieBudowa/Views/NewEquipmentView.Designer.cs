﻿namespace ZarzadzanieBudowa.Views
{
    partial class NewEquipmentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.cmbbx1 = new System.Windows.Forms.ComboBox();
            this.rtbx1 = new System.Windows.Forms.RichTextBox();
            this.lbl2 = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(12, 15);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(270, 32);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Wybierz kategorię";
            // 
            // cmbbx1
            // 
            this.cmbbx1.BackColor = System.Drawing.Color.LightBlue;
            this.cmbbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cmbbx1.FormattingEnabled = true;
            this.cmbbx1.Location = new System.Drawing.Point(288, 15);
            this.cmbbx1.Name = "cmbbx1";
            this.cmbbx1.Size = new System.Drawing.Size(304, 40);
            this.cmbbx1.TabIndex = 1;
            // 
            // rtbx1
            // 
            this.rtbx1.BackColor = System.Drawing.Color.LightBlue;
            this.rtbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbx1.Location = new System.Drawing.Point(288, 61);
            this.rtbx1.MaxLength = 255;
            this.rtbx1.Name = "rtbx1";
            this.rtbx1.Size = new System.Drawing.Size(304, 200);
            this.rtbx1.TabIndex = 2;
            this.rtbx1.Text = "";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl2.Location = new System.Drawing.Point(12, 61);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(270, 64);
            this.lbl2.TabIndex = 3;
            this.lbl2.Text = "Podaj krótki opis\r\n(max 255 znaków)";
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(18, 208);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(264, 50);
            this.btn1.TabIndex = 4;
            this.btn1.Text = "Dodaj sprzęt";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // NewEquipmentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(604, 271);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.rtbx1);
            this.Controls.Add(this.cmbbx1);
            this.Controls.Add(this.lbl1);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinimumSize = new System.Drawing.Size(620, 310);
            this.Name = "NewEquipmentView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj nowy sprzęt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.ComboBox cmbbx1;
        private System.Windows.Forms.RichTextBox rtbx1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Button btn1;
    }
}