﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class BuildingTaskContentView : Form
    {
        public BuildingTaskContentView(string tytul, string tresc)
        {
            InitializeComponent();
            label2.Text = tytul;
            richTextBox1.Text = tresc;
        }
    }
}
