﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;

namespace ZarzadzanieBudowa.Views
{
    public interface INewMaterialsView
    {
        DataTable Categories { get; set; } //Tabela zawierająca kategorie materiałów;
        string CategoriesDisplayMember { get; set; } //Nazwa kolumny, którą ma być wyświetlana w ComboBoxie;
        string CategoriesValueMember { get; set; } //Nazwa kolumny, którą ComboBox będzie zwracał;
        object CategoriesSelectedValue { get; } //Wybrana w ComboBoxie wartość;
        string Description { get; set; } //Opis nowego zamówienia;
        string Quantity { get; set; } //Ilość nowych materiałów;
        string Unit { get; set; } //Jednostka materiałów (zależna od kategorii);
        void SetController(NewMaterialsController controller); //Ustaw kontroler dla widoku;
    }
}
