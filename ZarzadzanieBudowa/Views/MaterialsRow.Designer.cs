﻿namespace ZarzadzanieBudowa.Views
{
    partial class MaterialsRow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Button();
            this.rtbx1 = new System.Windows.Forms.RichTextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.lbl4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(59, 3);
            this.lbl1.Margin = new System.Windows.Forms.Padding(3);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(297, 33);
            this.lbl1.TabIndex = 8;
            this.lbl1.Text = "0123456789012345";
            // 
            // lbl3
            // 
            this.lbl3.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl3.Location = new System.Drawing.Point(665, 3);
            this.lbl3.Margin = new System.Windows.Forms.Padding(3);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(135, 33);
            this.lbl3.TabIndex = 8;
            this.lbl3.Text = "01234567";
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl2.Location = new System.Drawing.Point(362, 3);
            this.lbl2.Margin = new System.Windows.Forms.Padding(3);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(297, 33);
            this.lbl2.TabIndex = 8;
            this.lbl2.Text = "0123456789012345";
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2.Location = new System.Drawing.Point(968, 3);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(145, 33);
            this.btn2.TabIndex = 9;
            this.btn2.Text = "Zaktualizuj ilość";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // rtbx1
            // 
            this.rtbx1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbx1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbx1.Location = new System.Drawing.Point(362, 0);
            this.rtbx1.MaximumSize = new System.Drawing.Size(297, 0);
            this.rtbx1.MaxLength = 255;
            this.rtbx1.MinimumSize = new System.Drawing.Size(297, 39);
            this.rtbx1.Name = "rtbx1";
            this.rtbx1.ReadOnly = true;
            this.rtbx1.Size = new System.Drawing.Size(297, 39);
            this.rtbx1.TabIndex = 10;
            this.rtbx1.Text = "";
            this.rtbx1.Visible = false;
            this.rtbx1.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.rtbx1_ContentsResized);
            // 
            // btn1
            // 
            this.btn1.BackgroundImage = global::ZarzadzanieBudowa.Properties.Resources.ArrowDown;
            this.btn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn1.Location = new System.Drawing.Point(20, 3);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(33, 33);
            this.btn1.TabIndex = 11;
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // lbl4
            // 
            this.lbl4.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl4.Location = new System.Drawing.Point(797, 3);
            this.lbl4.Margin = new System.Windows.Forms.Padding(3);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(165, 33);
            this.lbl4.TabIndex = 8;
            this.lbl4.Text = "0123456789";
            // 
            // MaterialsRow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.rtbx1);
            this.Controls.Add(this.lbl2);
            this.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(1136, 39);
            this.Name = "MaterialsRow";
            this.Size = new System.Drawing.Size(1136, 39);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.RichTextBox rtbx1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Label lbl4;
    }
}
