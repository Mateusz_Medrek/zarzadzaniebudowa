﻿namespace ZarzadzanieBudowa.Views
{
    partial class NewBuildingTasksListWorkerRowView
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.LabelDanePrcownika = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(277, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Obciążenie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LabelDanePrcownika
            // 
            this.LabelDanePrcownika.AllowDrop = true;
            this.LabelDanePrcownika.AutoSize = true;
            this.LabelDanePrcownika.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelDanePrcownika.Location = new System.Drawing.Point(0, 3);
            this.LabelDanePrcownika.Margin = new System.Windows.Forms.Padding(0);
            this.LabelDanePrcownika.MaximumSize = new System.Drawing.Size(260, 0);
            this.LabelDanePrcownika.MinimumSize = new System.Drawing.Size(260, 33);
            this.LabelDanePrcownika.Name = "LabelDanePrcownika";
            this.LabelDanePrcownika.Size = new System.Drawing.Size(260, 33);
            this.LabelDanePrcownika.TabIndex = 1;
            this.LabelDanePrcownika.Text = "label1";
            this.LabelDanePrcownika.DragEnter += new System.Windows.Forms.DragEventHandler(this.LabelDanePrcownika_DragEnter);
            this.LabelDanePrcownika.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LabelDanePrcownika_MouseDown);
            // 
            // PracownicyNowaListaZadanRow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.Controls.Add(this.LabelDanePrcownika);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(450, 0);
            this.MinimumSize = new System.Drawing.Size(450, 39);
            this.Name = "PracownicyNowaListaZadanRow";
            this.Size = new System.Drawing.Size(450, 39);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LabelDanePrcownika;
    }
}
