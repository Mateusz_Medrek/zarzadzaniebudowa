﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa.Views
{
    public interface IOrderRowView
    {
        int Id { get; set; } //Id zamówienia w BD;
        string CreateDate { get; set; } //Data utworzenia zamówienia;
        int StatusId { get; set; } //Id statusu zamówienia;
        string Status { get; set; } //Status zamówienia;
        string Description { get; set; } //Opis zamówienia;
        void Dispose(); //Zwolnij zasoby;
    }
}
