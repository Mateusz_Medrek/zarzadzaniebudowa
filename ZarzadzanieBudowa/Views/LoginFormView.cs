﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class LoginFormView : Form
    {
        public LoginFormView()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {//Metoda wykonywana po naciśnięciu przycisku Zaloguj;
            int id = -1;
            string login = "";
            int typ_uzytkownika = -1;
            //Sprawdzamy czy w bazie jest użytkownik o podanych danych;
            DataSet ds = DBConnection.SelectQuery("SELECT user_id, user_login, users_typ_id FROM users " +
                "WHERE user_login = @login AND user_password = @haslo", new DBParameter("@login", tbx1.Text), 
                new DBParameter("@haslo", tbx2.Text));
            if (ds == null)
            {
                MessageBox.Show("Coś poszło nie tak.");
            }
            else if (ds.Tables[0].Rows.Count >= 1)
            {//Jeśli zwrócona nam przynajmniej jeden wiersz to mamy takiego użytkownika;
                id = Convert.ToInt32(ds.Tables[0].Rows[0][0]); //Pobieramy id tego użytkownika;
                login = Convert.ToString(ds.Tables[0].Rows[0][1]); //Oraz jego login;
                typ_uzytkownika = Convert.ToInt32(ds.Tables[0].Rows[0][2]); //Oraz jego typ;
                this.Hide(); //"Zamykamy" (a tak naprawdę chowamy) okienko logowania;

                //     ------ Menu   ------
                ManagerMenuView mainMenu = new ManagerMenuView(login); //Tworzymy nowe okno z głównym menu;
                mainMenu.FormClosed += (s, args) => this.Close(); //Gdy główne menu zostanie zamknięte,
                                                                  //to zamykamy również schowane okno logowania;
                //     ------ Menu 2 ------
                EngineerMenuView mainMenu2 = new EngineerMenuView(login); //Tworzymy nowe okno z głównym menu;
                mainMenu2.FormClosed += (s, args) => this.Close(); //Gdy główne menu zostanie zamknięte,
                                                                   //to zamykamy również schowane okno logowania;
                //     ------ WorkersMenu ------
                WorkersMenuView workersMenu = new WorkersMenuView(); //Tworzymy nowe okno z głównym menu;
                workersMenu.FormClosed += (s, args) => this.Close(); //Gdy główne menu zostanie zamknięte,
                                                                   //to zamykamy również schowane okno logowania;

                if (typ_uzytkownika == 2) mainMenu.Show(); //Pokazujemy okno z głównym menu dla kierownika;
                if (typ_uzytkownika == 1) mainMenu2.Show(); //Pokazujemy okno z głównym menu dla inzyniera;
                if (typ_uzytkownika == 3) workersMenu.Show(); //Pokazujemy okno z głównym menu dla operatora;
            }
            else //Jeśli nie ma użytkownika o podanych danych w BD, pokazujemy stosowny komunikat;
            {
                MessageBox.Show("Niepoprawne dane logowania");
            }
        }

        private void tbx1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn1_Click(this, EventArgs.Empty);
                btn1.Focus();
            }
        }

        private void tbx2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn1_Click(this, EventArgs.Empty);
                btn1.Focus();
            }
        }
    }
}
