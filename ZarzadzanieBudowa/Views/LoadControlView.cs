﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZarzadzanieBudowa.Views
{
    public partial class LoadControlView : Form
    {
        public LoadControlView()
        {
            InitializeComponent();
        }
        private void ShowPracownicy()
        {
            #region Filling our panel with data
            //Wyszukaj wszystkich pracowników;
            DataSet ds = DBConnection.SelectQuery("SELECT * FROM pracownicy");
            //Wyczyść kontener;
            PracownicyTable.Controls.Clear();
            PracownicyTable.RowCount = 2;
            //Ustal co sięzdarzy po zajściu poniższych zdarzeń;
            PracownicyTable.ControlAdded += (s, args) => PracownicyTable.RowCount++;
            PracownicyTable.ControlRemoved += (s, args) => PracownicyTable.RowCount--;
            foreach (DataRow row in ds.Tables[0].Rows) //Dla każdego pracownika;
            {
                //Dodaj pracownika do kontenera;
                PracownicyTable.Controls.Add(new NewBuildingTasksListWorkerRowView
                {
                    Row_Id = row.Field<int>("pracownik_id"),
                    Dane = row.Field<string>("pracownik_imie") + ' ' + row.Field<string>("pracownik_nazwisko"),
                }, 0, PracownicyTable.RowCount - 1);
            }
            #endregion
        }
        private void NowaListaZadan_Load(object sender, EventArgs e)
        {
            ShowPracownicy();
        }

        private void SzukajTextBox_TextChanged(object sender, EventArgs e)
        {
            if (SzukajTextBox.Text == "")
            {
                foreach (NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
                {
                    //Dla każdego wyświetlanego wiersza z danymi materiałów ustaw jego widoczność;
                    r.Visible = true;
                }
                return;
            }
            foreach(NewBuildingTasksListWorkerRowView r in PracownicyTable.Controls)
            {
                //Dla każdego wyświetlanego wiersza z danymi materiałów ustaw jego widoczność;
                string nazwa = r.Dane.ToLower();
                if (nazwa.Contains(SzukajTextBox.Text.ToLower())) r.Visible = true;
                else r.Visible = false;
            }
        }
    }
}
