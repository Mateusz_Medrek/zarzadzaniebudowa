﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Controllers;
using ZarzadzanieBudowa.Models;

namespace ZarzadzanieBudowa.Views
{
    public interface IEquipmentView
    {
        List<IEquipmentRowView> EquipmentRowViews { get; set; } //Zbiór sprzętów jako elementów graficznych;
        List<string> Categories { get; set; } //Zbiór kategorii sprzętów;
        void SetController(EquipmentController controller); //Ustaw kontroler dla widoku;
        void AddEquipmentToView(Equipment equipment); //Dodaj sprzęt do widoku;
        void ClearView(); //Wyczyść widok;
        void RemoveEquipmentFromView(Equipment equipment); //Usuń sprzęt z widoku;
    }
}
