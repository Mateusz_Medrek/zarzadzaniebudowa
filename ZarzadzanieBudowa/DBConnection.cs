﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarzadzanieBudowa
{
    internal class DBConnection
    {
        //Nasz connection string do bazy db4free;
        static private readonly string dbconn = "server=db4free.net;database=janusz_budowa;" +
            "uid=janusz;pwd=budowaioio69;SslMode=None;old guids=true;Connection Timeout=30;";

        //Nasz connection string do bazy mysqlfreehosting;
        //static private readonly string dbconn = "server=sql7.freemysqlhosting.net;database=sql7273397;" + 
        //    "uid=sql7273397;pwd=EfGyEeENnA;SslMode=None;old guids=true;Connection Timeout=30;";

        static internal DataSet SelectQuery(string query, params DBParameter[] parameters)
        {//Metoda wykonująca zapytania SELECT; Przyjmuje treść zamówienia i dowolną nieujemną liczbę parametrów ;
            try
            {
                DataSet ds = new DataSet(); //Zbiór danych (np. kilka tabel);
                using (MySqlConnection conn = new MySqlConnection(dbconn))
                {//Ustawiamy połączenie z serwerem bazy danych;
                    conn.Open(); //Otwieramy połączenie;
                    using (MySqlCommand command = new MySqlCommand(query, conn))
                    {//Ustawiamy komendę bazodanową; Podajemy treść i połączenie do BD;
                        for (int i = 0; i < parameters.Count(); i++)
                        {//Podajemy wszystkie parametry;
                            command.Parameters.AddWithValue(parameters[i].Name, parameters[i].Value);
                        }
                        command.Prepare();                                        //Wysyłamy zapytanie, dostajemy
                        MySqlDataAdapter adapter = new MySqlDataAdapter(command); //wynik, który przekazujemy
                        adapter.Fill(ds);                                         //do naszego zbioru danych;
                    }
                }
                return ds; //Zwracamy zbiór danych;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        static internal int NonQuery(string query, params DBParameter[] parameters)
        {//Metoda wykonująca zapytania UPDATE, INSERT, DELETE;
            try
            {
                int tmp = -1; //Ilość zmienionych rekordów;
                using (MySqlConnection conn = new MySqlConnection(dbconn))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(query, conn))
                    {
                        for (int i = 0; i < parameters.Count(); i++)
                        {
                            command.Parameters.AddWithValue(parameters[i].Name, parameters[i].Value);
                        }
                        command.Prepare();               //Wykonujemy zapytanie, zwrócona zostaje nam ilość
                        tmp = command.ExecuteNonQuery(); //zmienionych rekordów w bazie;
                    }
                }
                return tmp;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}
