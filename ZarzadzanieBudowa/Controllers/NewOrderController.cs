﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class NewOrderController
    {
        private INewOrderView newOrderView; //Widok dla kontrolera;

        public NewOrderController(INewOrderView newOrderView)
        {
            this.newOrderView = newOrderView; //Ustaw widok dla kontrolera;
            this.newOrderView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
        }

        public int AddToDB() //Dodaj nowy sprzęt do BD;
        {
            //Ustaw datę utworzenia na TERAZ;
            DateTime dt = DateTime.Now;
            //Wstaw nowe zamówienie do BD;
            return DBConnection.NonQuery("INSERT INTO zamowienia (zamowienie_opis, " +
                "zamowienie_du, zamowienie_status_id) VALUES (@opis, @du, @status)",
                new DBParameter("@opis", newOrderView.Description),
                new DBParameter("@du", dt.Year + "-" + dt.Month + "-" + dt.Day),
                new DBParameter("@status", 1));
        }

        public int EditInDB() //Edytuj zamówienie w BD;
        {
            //Edytuj zamówienie w BD;
            return DBConnection.NonQuery("UPDATE zamowienia SET zamowienie_opis = @opis " +
                "WHERE zamowienie_id = @id", new DBParameter("@opis", newOrderView.Description),
                new DBParameter("@id", newOrderView.Id));
        }
    }
}
