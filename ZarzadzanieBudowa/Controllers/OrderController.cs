﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Models;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class OrderController
    {
        private IOrderView orderView; //Widok dla kontrolera;

        public List<Order> Orders { get; set; } //Zbiór zamówień;

        public bool DbDead { get; private set; }

        public OrderController(IOrderView orderView)
        {
            this.orderView = orderView; //Ustaw widok dla kontrolera;
            this.orderView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            DbDead = false;
            LoadData(); //Ładuj dane z BD;
        }

        public void AddOrder(Order order) //Dodaj zamówienie;
        {
            Orders.Add(order); //Dodaj do modelu;
            orderView.AddOrderToView(order); //Dodaj do widoku;
        }

        public int CancelOrder(IOrderRowView orderRowView) //Anuluj zamówienie;
        {
            foreach (Order order in Orders)
            {
                if (order.Id == orderRowView.Id)
                {
                    //Zmień status na anulowane;
                    //Baza;
                    int tmp = DBConnection.NonQuery("UPDATE zamowienia SET zamowienie_status_id = 4 " +
                        "WHERE zamowienie_id = @id", new DBParameter("@id", order.Id));
                    //Model;
                    if (tmp >= 1)
                    {
                        order.StatusId = orderRowView.StatusId = 4;
                        order.Status = orderRowView.Status = "anulowane";
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public void UpdateModelValues(IOrderRowView orderRowView) //Uaktualnij wartości w modelu;
        {
            foreach (Order order in Orders) //Dla każdego zamówienia z modelu;
            {
                if (order.Id == orderRowView.Id) //Jeśli znajdziesz szukane zamówienie;
                {
                    //Uaktualnij bazę;
                    int tmp = DBConnection.NonQuery("UPDATE zamowienia SET zamowienie_opis = @opis " +
                        "WHERE zamowienie_id = @id", new DBParameter("@opis", orderRowView.Description),
                        new DBParameter("@id", orderRowView.Id));
                    if (tmp >= 1) order.Description = orderRowView.Description; //Uaktualnij model;
                    break;
                }
            }
        }

        public int RemoveOrder(IOrderRowView orderRowView) //Usuń zamówienie z modelu;
        {
            foreach (Order order in Orders) //Dla każdego zamówienia z modelu;
            {
                if (order.Id == orderRowView.Id) //Jeśli znajdziesz szukane zamówienie;
                {
                    //Usuń z BD;
                    int tmp = DBConnection.NonQuery("DELETE FROM zamowienia WHERE zamowienie_id = @id",
                        new DBParameter("@id", Convert.ToInt32(order.Id)));
                    if (tmp >= 1) //Jeśli się udało;
                    {
                        orderView.RemoveOrderFromView(order); //Usuń z widoku;
                        Orders.Remove(order); //Usuń z modelu;
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public void LoadData() //Ładuj z BD;
        {
            //Wyszukaj wszystkie zamówienia z BD;
            DataTable dt = DBConnection.SelectQuery("SELECT zamowienie_id, zamowienie_du, zamowienie_opis, " +
                "zamowienie_status_id, status FROM zamowienia INNER JOIN zamowienia_status " +
                "USING (zamowienie_status_id)").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            Orders = new List<Order>(); //Ustaw zbiór zamówień;
            foreach (DataRow row in dt.Rows) //Dla każdego rekordu;
            {
                //Dodaj zamówienie;
                AddOrder(new Order
                {
                    Id = row.Field<int>("zamowienie_id"),
                    CreateDate = row.Field<DateTime>("zamowienie_du").ToShortDateString(),
                    Description = row.Field<string>("zamowienie_opis"),
                    StatusId = row.Field<int>("zamowienie_status_id"),
                    Status = row.Field<string>("status")
                });
            }
            LoadFilter(); //Ładuj dane do filtra;
            if (!DbDead) LoadView(); //Ładuj widok;
        }

        public void LoadFilter()
        {
            //Wyszukaj wszystkie możliwe statusy zamówień;
            DataTable dt = DBConnection.SelectQuery("SELECT * FROM zamowienia_status " +
                "ORDER BY status").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            //Ustaw zbiór możliwych statusów;
            orderView.Categories = new List<string>();
            foreach (DataRow row in dt.Rows) orderView.Categories.Add(row.Field<string>("status"));
        }

        public void LoadView() //Ładuj widok;
        {
            orderView.ClearView(); //Wyczyść widok;
            //Ładuj z modelu do widoku;
            foreach (Order o in Orders) orderView.AddOrderToView(o);
        }
    }
}
