﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class NewMaterialsController
    {
        private INewMaterialsView newMaterialsView; //Widok dla kontrolera;
        private DataTable dt; //Źródło danych dla ComboBoxa;

        public bool DbDead { get; private set; }

        public NewMaterialsController(INewMaterialsView newMaterialsView)
        {
            this.newMaterialsView = newMaterialsView; //Ustaw widok dla kontrolera;
            this.newMaterialsView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
        }

        public void LoadComboBox() //Ładuj dane do ComboBoxa;
        {
            //Wyszukaj wszystkie kategorie dla materiałów;
            dt = DBConnection.SelectQuery("SELECT * FROM materialy_kategoria " +
                "ORDER BY kategoria").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            newMaterialsView.Categories = dt; //Ustaw kategorie;
            newMaterialsView.CategoriesDisplayMember = "kategoria"; //Ustaw co wyświetlasz;
            newMaterialsView.CategoriesValueMember = "materialy_kategoria_id"; //Ustaw co zwrócisz;
        }

        public void ChangeUnit(int selectedValue) //Zmień jednostkę;
        {
            //Znajdź we wcześniej wyszukanych danych, jednostki dla wybranego rekordu;
            DataRow[] tmp = dt.Select(String.Format("materialy_kategoria_id = {0}", selectedValue));
            newMaterialsView.Unit = Convert.ToString(tmp[0][2]); //Przypisz wyświetlaną jednostkę;
        }

        public int AddToDB() //Dodaj nowe materiały do BD;
        {
            //Wstaw nowe materiały do BD;
            return DBConnection.NonQuery("INSERT INTO materialy (materialy_kategoria_id, " + 
                "materialy_opis, materialy_ilosc) VALUES (@kategoria, @opis, @ilosc)", 
                new DBParameter("@kategoria", Convert.ToInt32(newMaterialsView.CategoriesSelectedValue)), 
                new DBParameter("@opis", newMaterialsView.Description), 
                new DBParameter("@ilosc", Convert.ToDecimal(newMaterialsView.Quantity)));
        }
    }
}
