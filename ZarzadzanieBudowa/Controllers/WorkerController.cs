﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Models;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class WorkerController
    {
        private IWorkersView workersView;

        public List<Worker> Workers { get; set; }

        public bool DbDead { get; private set; }

        public WorkerController(IWorkersView workersView)
        {
            this.workersView = workersView;
            this.workersView.SetController(this);
            LoadData();
        }

        public void AddWorker(Worker worker) //Dodaj pracownika;
        {
            Workers.Add(worker); //Dodaj do modelu;
            workersView.AddWorkerToView(worker); //Dodaj do widoku;
        }

        public int UpdateModelValues(IWorkersRowView workersRowView)
        {
            foreach (Worker worker in Workers)
            {
                if (worker.Id == workersRowView.Id)
                {
                    //Uaktualnij bazę;
                    int tmp = DBConnection.NonQuery("UPDATE pracownicy set pracownik_imie = @imie, " + 
                        "pracownik_nazwisko = @nazwisko, pracownik_pesel = @pesel, " + 
                        "pracownik_data_zatrudnienia = @data, pracownik_status_id = @status " +
                        "WHERE pracownik_id = @id", 
                        new DBParameter("@imie", workersRowView.WorkerName), 
                        new DBParameter("@nazwisko", workersRowView.WorkerSurname), 
                        new DBParameter("@pesel", workersRowView.Pesel), 
                        new DBParameter("@data", Convert.ToDateTime(workersRowView.EmploymentDate)),
                        new DBParameter("@status", workersRowView),
                        new DBParameter("@id", worker.Id));
                    if (tmp >= 1)
                    {
                        //Uaktualnij model;
                        worker.WorkerName = workersRowView.WorkerName;
                        worker.WorkerSurname = workersRowView.WorkerSurname;
                        worker.Pesel = workersRowView.Pesel;
                        worker.EmploymentDate = workersRowView.EmploymentDate;
                        worker.Status = workersRowView.Status;
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public int RemoveWorker(IWorkersRowView workersRowView) //Usuń pracownika z modelu;
        {
            foreach (Worker worker in Workers) //Dla każdego pracownika z modelu;
            {
                if (worker.Id == workersRowView.Id) //Jeśli znajdziesz szukanego pracownika;
                {
                    //Usuń z BD;
                    int tmp = DBConnection.NonQuery("DELETE FROM pracownicy WHERE pracownik_id = @id", 
                        new DBParameter("@id", worker.Id));
                    if (tmp >= 1) //Jeśli się udało;
                    {
                        workersView.RemoveWorkerFromView(worker); //Usuń pracownika z widoku;
                        Workers.Remove(worker); //Usuń z modelu;
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public void LoadComboBox(INewEditWorkersView newEditWorkersView)
        {
            DataTable dt = DBConnection.SelectQuery("SELECT * FROM pracownicy_status " +
                "ORDER BY pracownik_status").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            newEditWorkersView.Categories = dt;
            newEditWorkersView.CategoriesDisplayMember = "pracownik_status";
            newEditWorkersView.CategoriesValueMember = "pracownik_status_id";
        }

        public void LoadData() //Ładuj z BD;
        {
            //Wyszukaj wszystkich pracowników z BD;
            DataTable dt = DBConnection.SelectQuery("SELECT pracownik_id, pracownik_imie, pracownik_nazwisko, " +
                "pracownik_pesel, pracownik_data_zatrudnienia, pracownik_status_id, pracownik_status " +
                "FROM pracownicy INNER JOIN pracownicy_status USING (pracownik_status_id)").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            Workers = new List<Worker>(); //Ustaw zbiór pracowników;
            foreach (DataRow row in dt.Rows) //Dla każdego rekordu;
            {
                //Dodaj pracownika;
                AddWorker(new Worker
                {
                    Id = row.Field<int>("pracownik_id"),
                    WorkerName = row.Field<string>("pracownik_imie"),
                    WorkerSurname = row.Field<string>("pracownik_nazwisko"),
                    Pesel = row.Field<string>("pracownik_pesel"),
                    EmploymentDate = row.Field<DateTime>("pracownik_data_zatrudnienia").ToShortDateString(),
                    Status = row.Field<string>("pracownik_status")
                });
            }
            LoadFilter(); //Ładuj dane do filtra;
            if (!DbDead) LoadView(); //Ładuj widok;
        }

        public void LoadFilter() //Ładuj dane do filtra;
        {
            //Wyszukaj kategorie sprzętów;
            DataTable dt = DBConnection.SelectQuery("SELECT pracownik_status FROM pracownicy_status " +
                "ORDER BY pracownik_status").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            //Ustaw zbiór kategorii sprzętu;
            workersView.Categories = new List<string>();
            foreach (DataRow row in dt.Rows)
                workersView.Categories.Add(row.Field<string>("pracownik_status"));
        }

        public void LoadView() //Ładuj widok;
        {
            workersView.ClearView(); //Wyczyść widok;
            //Ładuj z modelu do widoku;
            foreach (Worker w in Workers) workersView.AddWorkerToView(w);
        }
    }
}
