﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Models;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class ReportController
    {
        private IReportView reportView; //Widok dla kontrolera;
        private Report report;

        public ReportController(IReportView reportView)
        {
            this.reportView = reportView; //Ustaw widok dla kontrolera;
            this.reportView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            report = new Report(); //Nowa instancja modelu;
        }

        public int UpdateModelValues()
        {
            //Zaktualizuj model;
            report.Author = reportView.Author;
            report.Content = reportView.Content;
            report.CreateDate = reportView.CreateDate;
            report.ReportName = reportView.ReportName;
            //Zaktualizuj BD;
            return DBConnection.NonQuery("INSERT INTO raporty (tytul, " + 
                "opis, autor, data) VALUES (@tytul, " + 
                "@opis, @autor, @data)", new DBParameter("@tytul", report.ReportName), 
                new DBParameter("@opis", report.Content), new DBParameter("@autor", report.Author), 
                new DBParameter("@data", report.CreateDate.Year + "-" + report.CreateDate.Month + 
                "-" + report.CreateDate.Day));
        }
    }
}
