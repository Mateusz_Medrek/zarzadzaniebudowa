﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Models;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class EquipmentController
    {
        private IEquipmentView equipmentView; //Widok dla kontrolera;

        public List<Equipment> Equipments { get; set; } //Zbiór sprzętów;

        public bool DbDead { get; private set; } //Czy baza danych przestała działać;

        public EquipmentController(IEquipmentView equipmentView)
        {
            this.equipmentView = equipmentView; //Ustaw widok dla kontrolera;
            this.equipmentView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            DbDead = false;
            LoadData(); //Ładuj dane z BD;
        }

        public void AddEquipment(Equipment equipment) //Dodaj sprzęt;
        {
            Equipments.Add(equipment); //Dodaj do modelu;
            equipmentView.AddEquipmentToView(equipment); //Dodaj do widoku;
        }

        public int UpdateModelValues(IEquipmentRowView equipmentRowView) //Uaktualnij wartości w modelu;
        {
            foreach (Equipment equipment in Equipments) //Dla każdego zamówienia z modelu;
            {
                if (equipment.Id == equipmentRowView.Id) //Jeśli znajdziesz szukane zamówienie;
                {
                    //Uaktualnij BD;
                    int tmp = DBConnection.NonQuery("UPDATE sprzet SET sprzet_czy_dostepny = @czy_dostepny " +
                        "WHERE sprzet_id = @id", new DBParameter("@czy_dostepny", equipmentRowView.IsAvailable), 
                        new DBParameter("@id", Convert.ToInt32(equipmentRowView.Id)));
                    if (tmp >= 1) equipment.IsAvailable = equipmentRowView.IsAvailable; //Uaktualnij model;
                    return tmp;
                }
            }
            return -1;
        }

        public int RemoveEquipment(IEquipmentRowView equipmentRowView) //Usuń sprzęt z modelu;
        {
            foreach (Equipment equipment in Equipments) //Dla każdego sprzętu z modelu;
            {
                if (equipment.Id == equipmentRowView.Id) //Jeśli znajdziesz szukany sprzęt;
                {
                    //Usuń w BD;
                    int tmp = DBConnection.NonQuery("DELETE FROM sprzet WHERE sprzet_id = @id",
                        new DBParameter("@id", Convert.ToInt32(equipmentRowView.Id)));
                    if (tmp >= 1) //Jeśli się udało;
                    {
                        equipmentView.RemoveEquipmentFromView(equipment); //Usuń z widoku;
                        Equipments.Remove(equipment); //Usuń z modelu;
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public void LoadData() //Ładuj z BD;
        {
            //Wyszukaj wszystkie sprzęty z BD;
            DataTable dt = DBConnection.SelectQuery("SELECT sprzet_id, sprzet_kategoria_id, kategoria, " +
                "sprzet_opis, sprzet_czy_dostepny FROM sprzet INNER JOIN sprzet_kategoria USING " +
                "(sprzet_kategoria_id)").Tables[0];
            if (dt == null) //Jeśli zapytanie z bazy nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            Equipments = new List<Equipment>(); //Ustaw zbiór sprzętów;
            foreach (DataRow row in dt.Rows) //Dla każdego rekordu;
            {
                //Dodaj sprzęt;
                AddEquipment(new Equipment
                {
                    Id = row.Field<int>("sprzet_id"),
                    Category = row.Field<string>("kategoria"),
                    Description = row.Field<string>("sprzet_opis"),
                    IsAvailable = row.Field<bool>("sprzet_czy_dostepny")
                });
            }
            LoadFilter(); //Ładuj dane do filtra;
            if (!DbDead) LoadView(); //Ładuj widok;
        }

        public void LoadFilter() //Ładuj dane do filtra;
        {
            //Wyszukaj kategorie sprzętów;
            DataTable dt = DBConnection.SelectQuery("SELECT * FROM sprzet_kategoria " +
                "ORDER BY kategoria").Tables[0];
            if (dt == null) //Jeśli zapytanie z bazy nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            //Ustaw zbiór kategorii sprzętu;
            equipmentView.Categories = new List<string>();
            foreach (DataRow row in dt.Rows) equipmentView.Categories.Add(row.Field<string>("kategoria"));
        }

        public void LoadView() //Ładuj widok;
        {
            equipmentView.ClearView(); //Wyczyść widok;
            //Ładuj z modelu do widoku;
            foreach (Equipment s in Equipments) equipmentView.AddEquipmentToView(s);
        }
    }
}
