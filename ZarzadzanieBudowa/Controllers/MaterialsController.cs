﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Models;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class MaterialsController
    {
        private IMaterialsView materialsView; //Widok dla kontrolera;

        public List<Materials> Materials { get; set; } //Zbiór materiałów;

        public bool DbDead { get; private set; } //Czy baza danych przestała działać;

        public MaterialsController(IMaterialsView materialsView)
        {
            this.materialsView = materialsView; //Ustaw widok dla kontrolera;
            this.materialsView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            DbDead = false;
            LoadData(); //Ładuj dane z BD;
        }

        public void AddMaterials(Materials material) //Dodaj materiały;
        {
            Materials.Add(material); //Dodaj do modelu;
            materialsView.AddMaterialsToView(material); //Dodaj do widoku;
        }
        
        public int UpdateModelValues(IMaterialsRowView materialsRowView) //Uaktualnij wartości w modelu;
        {
            foreach (Materials material in Materials) //Dla każdych materiałów z modelu;
            {
                if (material.Id == materialsRowView.Id) //Jeśli znajdziesz szukane materiały;
                {
                    //Uaktualnij bazę;
                    int tmp = DBConnection.NonQuery("UPDATE materialy SET materialy_ilosc = @ilosc WHERE " +
                        "materialy_id = @id", new DBParameter("@ilosc", Convert.ToDecimal(materialsRowView.Quantity)), 
                        new DBParameter("@id", materialsRowView.Id));
                    if (tmp >= 1) material.Quantity = materialsRowView.Quantity; //Uaktualnij model;
                    return tmp;
                }
            }
            return -1;
        }

        public int RemoveMaterials(IMaterialsRowView materialsRowView) //Usuń materiały z modelu;
        {
            foreach (Materials material in Materials) //Dla każdych materiałów z modelu;
            {
                if (material.Id == materialsRowView.Id) //Jeśli znajdziesz szukane materiały;
                {
                    //Usuń z BD;
                    int tmp = DBConnection.NonQuery("DELETE FROM materialy WHERE materialy_id = @id",
                        new DBParameter("@id", materialsRowView.Id));
                    if (tmp >= 1) //Jeśli się udało;
                    {
                        materialsView.RemoveMaterialsFromView(material); //Usuń z widoku;
                        Materials.Remove(material); //Usuń z modelu;
                    }
                    return tmp;
                }
            }
            return -1;
        }

        public void LoadData() //Ładuj z BD;
        {
            //Wyszukaj wszystkie materiały z BD;
            DataTable dt = DBConnection.SelectQuery("SELECT materialy_id, materialy_kategoria_id, kategoria, " +
                "jednostka, materialy_opis, materialy_ilosc FROM materialy INNER JOIN " +
                "materialy_kategoria USING (materialy_kategoria_id)").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            Materials = new List<Materials>(); //Ustaw zbiór materiałów;
            foreach (DataRow row in dt.Rows) //Dla każdego rekordu;
            {
                //Dodaj materiały;
                AddMaterials(new Materials
                {
                    Id = row.Field<int>("materialy_id"),
                    Category = row.Field<string>("kategoria"),
                    Description = row.Field<string>("materialy_opis"),
                    Quantity = Convert.ToString(row.Field<decimal>("materialy_ilosc")),
                    Unit = row.Field<string>("jednostka")
                });
            }
            LoadFilter(); //Ładuj dane do filtra;
            if (!DbDead) LoadView(); //Ładuj widok;
        }

        public void LoadFilter() //Ładuj dane do filtra;
        {
            //Wyszukaj kategorie sprzętów;
            DataTable dt = DBConnection.SelectQuery("SELECT kategoria FROM materialy_kategoria " +
                "ORDER BY kategoria").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            //Ustaw zbiór kategorii sprzętu;
            materialsView.Categories = new List<string>();
            foreach (DataRow row in dt.Rows) materialsView.Categories.Add(row.Field<string>("kategoria"));
        }

        public void LoadView() //Ładuj widok;
        {
            materialsView.ClearView(); //Wyczyść widok;
            //Ładuj z modelu do widoku;
            foreach (Materials m in Materials) materialsView.AddMaterialsToView(m);
        }
    }
}
