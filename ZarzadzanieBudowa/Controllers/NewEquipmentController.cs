﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class NewEquipmentController
    {
        private INewEquipmentView newEquipmentView; //Widok dla kontrolera;

        public bool DbDead { get; private set; }

        public NewEquipmentController(INewEquipmentView newEquipmentView)
        {
            this.newEquipmentView = newEquipmentView; //Ustaw widok dla kontrolera;
            this.newEquipmentView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            DbDead = false;
        }

        public void LoadComboBox() //Ładuj dane do ComboBoxa;
        {
            //Wyszukaj wszystkie kategorie dla sprzętów;
            DataTable dt = DBConnection.SelectQuery("SELECT * FROM sprzet_kategoria " +
                "ORDER BY kategoria").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            newEquipmentView.Categories = dt; //Ustaw kategorie;
            newEquipmentView.CategoriesDisplayMember = "kategoria"; //Ustaw co wyświetlasz;
            newEquipmentView.CategoriesValueMember = "sprzet_kategoria_id"; //Ustaw co zwrócisz;
        }

        public int AddToDB(INewEquipmentView newEquipmentView) //Dodaj nowy sprzęt do BD;
        {
            //Wstaw nowy sprzet do BD;
            return DBConnection.NonQuery("INSERT INTO sprzet (sprzet_kategoria_id, sprzet_opis, " +
                "sprzet_czy_dostepny) VALUES (@kategoria, @opis, @czy_dostepny)",
                new DBParameter("@kategoria", Convert.ToString(newEquipmentView.CategoriesSelectedValue)),
                new DBParameter("@opis", newEquipmentView.Description), 
                new DBParameter("@czy_dostepny", true));
        }
    }
}
