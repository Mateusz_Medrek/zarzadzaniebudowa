﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarzadzanieBudowa.Views;

namespace ZarzadzanieBudowa.Controllers
{
    public class NewEditWorkerController
    {
        private INewEditWorkersView newEditWorkersView; //Widok dla kontrolera;
        private DataTable dt; //Źrodło danych do ComboBoxa;

        public bool DbDead { get; private set; }

        public NewEditWorkerController(INewEditWorkersView newEditWorkersView)
        {
            this.newEditWorkersView = newEditWorkersView;
            this.newEditWorkersView.SetController(this); //Ustaw dla tego widoku TEN kontroler;
            LoadComboBox(); //Załaduj dane do ComboBoxa;
        }

        public void LoadComboBox()
        {
            dt = DBConnection.SelectQuery("SELECT * FROM pracownicy_status " +
                "ORDER BY pracownik_status").Tables[0];
            if (dt == null) //Jeśli zapytanie nic nie zwróciło;
            {
                DbDead = true; //Problem z serwerem BD;
                return; //Anuluj dalsze wczytywanie;
            }
            newEditWorkersView.Categories = dt; //Ustaw statusy;
            newEditWorkersView.CategoriesDisplayMember = "pracownik_status"; //Ustaw co wyświetlasz;
            newEditWorkersView.CategoriesValueMember = "pracownik_status_id"; //Ustaw co zwrócisz;
        }

        public int AddToDB(INewEditWorkersView newEditWorkersView)
        {
            return DBConnection.NonQuery("INSERT INTO pracownicy (pracownik_imie, pracownik_nazwisko, " +
                "pracownik_pesel, pracownik_data_zatrudnienia, pracownik_status_id) VALUES (@imie, " +
                "@nazwisko, @pesel, @data_zatrudnienia, @status)", 
                new DBParameter("@imie", newEditWorkersView.WorkerName),
                new DBParameter("@nazwisko", newEditWorkersView.WorkerSurname), 
                new DBParameter("@pesel", newEditWorkersView.Pesel),
                new DBParameter("@data_zatrudnienia", newEditWorkersView.EmploymentDate.Year + "-" +
                newEditWorkersView.EmploymentDate.Month + "-" + newEditWorkersView.EmploymentDate.Day),
                new DBParameter("@status", Convert.ToInt32(newEditWorkersView.CategoriesSelectedValue)));
        }

        public int UpdateInDB(INewEditWorkersView newEditWorkersView)
        {
            return DBConnection.NonQuery("UPDATE pracownicy set pracownik_imie = @imie, " +
                "pracownik_nazwisko = @nazwisko, pracownik_pesel = @pesel, " +
                "pracownik_data_zatrudnienia = @data, pracownik_status_id = @status " +
                "WHERE pracownik_id = @id",
                new DBParameter("@imie", newEditWorkersView.WorkerName),
                new DBParameter("@nazwisko", newEditWorkersView.WorkerSurname),
                new DBParameter("@pesel", newEditWorkersView.Pesel),
                new DBParameter("@data", Convert.ToDateTime(newEditWorkersView.EmploymentDate)),
                new DBParameter("@status", Convert.ToInt32(newEditWorkersView.CategoriesSelectedValue)),
                new DBParameter("@id", newEditWorkersView.Id));
        }
    }
}
