-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 22 Sty 2019, 22:28
-- Wersja serwera: 8.0.13
-- Wersja PHP: 7.2.10-0ubuntu0.18.04.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `janusz_budowa`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `listy_zadan`
--

DROP TABLE IF EXISTS `listy_zadan`;
CREATE TABLE IF NOT EXISTS `listy_zadan` (
  `lista_zadan_id` int(11) NOT NULL AUTO_INCREMENT,
  `lista_zadan_nazwa` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `lista_zadan_dom` date NOT NULL,
  `lista_zadan_dr` date NOT NULL,
  `lista_zadan_du` date NOT NULL,
  PRIMARY KEY (`lista_zadan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `listy_zadan`
--

INSERT INTO `listy_zadan` (`lista_zadan_id`, `lista_zadan_nazwa`, `lista_zadan_dom`, `lista_zadan_dr`, `lista_zadan_du`) VALUES
(1, 'lista_zadan_nr1', '2018-11-12', '2018-11-13', '2018-11-10'),
(2, 'zadanka_na_wczoraj', '2018-11-18', '2018-11-19', '2018-11-13'),
(3, 'Kocie zadania', '2019-01-07', '2018-12-24', '2019-01-07'),
(5, 'skrypty', '2019-01-22', '2019-01-17', '2019-01-16'),
(6, 'trallala', '2019-01-22', '2019-01-25', '2019-01-16'),
(7, 'PatIKot', '2019-01-21', '2019-01-16', '2019-01-16'),
(8, 'Bob i kot', '2019-01-22', '2019-01-16', '2019-01-16'),
(9, '', '2019-01-16', '2019-01-16', '2019-01-16'),
(10, '', '2019-01-16', '2019-01-16', '2019-01-16'),
(11, 'testowa', '2019-01-17', '2019-01-17', '2019-01-17'),
(12, 'czarna lista', '2019-01-17', '2019-01-17', '2019-01-17'),
(13, 'kurczaki', '2019-01-22', '2019-01-17', '2019-01-17'),
(14, 'pusta', '2019-01-17', '2019-01-17', '2019-01-17'),
(15, '', '2019-01-17', '2019-01-17', '2019-01-17'),
(16, '', '2019-01-17', '2019-01-17', '2019-01-17'),
(17, 'zadanka_na_wczoraj', '2019-01-17', '2018-11-19', '2019-01-17'),
(18, 'kuce', '2019-01-18', '2019-01-18', '2019-01-18'),
(19, 'Grafika', '2019-01-18', '2019-01-18', '2019-01-18'),
(20, 'Fineasz i Ferb', '2019-01-18', '2019-01-22', '2019-01-18'),
(21, 'testowanie', '2019-01-22', '2019-01-22', '2019-01-22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `materialy`
--

DROP TABLE IF EXISTS `materialy`;
CREATE TABLE IF NOT EXISTS `materialy` (
  `materialy_id` int(11) NOT NULL AUTO_INCREMENT,
  `materialy_kategoria_id` int(11) NOT NULL,
  `materialy_opis` tinytext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `materialy_ilosc` decimal(6,2) NOT NULL,
  PRIMARY KEY (`materialy_id`),
  KEY `materialy_kategoria_id` (`materialy_kategoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `materialy`
--

INSERT INTO `materialy` (`materialy_id`, `materialy_kategoria_id`, `materialy_opis`, `materialy_ilosc`) VALUES
(1, 1, 'Kostka brukowa firmy PolBruk', '15.50'),
(3, 3, 'Zaprawa montażowa cx20 firmy CERESIT', '40.00'),
(4, 3, 'Zaprawa klejowa cx5 firmy CERESIT', '48.00'),
(5, 3, 'Cement firmy Polski Cement', '15.00'),
(6, 2, 'Beton towarowy firmy BostaBeton', '5000.00'),
(7, 2, 'Beton firmy PolBeton', '3500.00'),
(8, 2, 'Beton firmy BetonPol', '3000.00'),
(10, 3, 'Cement marki cement', '28.00'),
(11, 3, 'Cementu marki cementPOL', '30.00'),
(12, 1, 'Jakaś kostka brukowa', '8.00'),
(13, 3, 'Cement marki  AmpliMagaRex', '20.00'),
(15, 2, 'Beton na sterydach', '1000.00'),
(16, 2, 'Testowy beton', '2500.00'),
(21, 2, '11111111111', '111.00'),
(22, 2, 'twardy', '90.00'),
(23, 2, 'mientki', '90.00'),
(25, 2, 'ddddd', '5.00'),
(26, 3, 'adsfasdf', '3.00'),
(27, 3, 'test1', '45.50'),
(28, 1, 'fajna taka kwadratowa', '10.22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `materialy_kategoria`
--

DROP TABLE IF EXISTS `materialy_kategoria`;
CREATE TABLE IF NOT EXISTS `materialy_kategoria` (
  `materialy_kategoria_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategoria` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `jednostka` varchar(10) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`materialy_kategoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `materialy_kategoria`
--

INSERT INTO `materialy_kategoria` (`materialy_kategoria_id`, `kategoria`, `jednostka`) VALUES
(1, 'kostka_brukowa', 'paleta'),
(2, 'beton', 'kg/m3'),
(3, 'cement', 'kg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

DROP TABLE IF EXISTS `pracownicy`;
CREATE TABLE IF NOT EXISTS `pracownicy` (
  `pracownik_id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_imie` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pracownik_nazwisko` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pracownik_pesel` char(11) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pracownik_data_zatrudnienia` date NOT NULL,
  `pracownik_status_id` int(11) NOT NULL,
  PRIMARY KEY (`pracownik_id`),
  KEY `pracownik_status_id` (`pracownik_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`pracownik_id`, `pracownik_imie`, `pracownik_nazwisko`, `pracownik_pesel`, `pracownik_data_zatrudnienia`, `pracownik_status_id`) VALUES
(25, 'Kot', 'Paczacz', '69696969696', '2019-01-14', 4),
(31, 'Dziunia', 'Najlepsza', '12312312369', '2019-01-19', 1),
(32, 'Bimkie', 'Guy', '12321453678', '2019-01-22', 3),
(33, 'Bumfight', 'Sparkle', '12345678900', '2019-01-22', 1),
(34, 'Flubbershy', 'motylek', '00987654321', '2019-01-22', 1),
(35, 'Bapple', 'Jack', '54637281901', '2019-01-22', 1),
(36, 'Brzydal', 'Ropuch', '03948475621', '2019-01-22', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy_status`
--

DROP TABLE IF EXISTS `pracownicy_status`;
CREATE TABLE IF NOT EXISTS `pracownicy_status` (
  `pracownik_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_status` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`pracownik_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pracownicy_status`
--

INSERT INTO `pracownicy_status` (`pracownik_status_id`, `pracownik_status`) VALUES
(1, 'dostepny'),
(2, 'l4'),
(3, 'szkolenie'),
(4, 'urlop');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `raporty`
--

DROP TABLE IF EXISTS `raporty`;
CREATE TABLE IF NOT EXISTS `raporty` (
  `index` int(3) NOT NULL AUTO_INCREMENT,
  `tytul` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `opis` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `autor` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `raporty`
--

INSERT INTO `raporty` (`index`, `tytul`, `opis`, `autor`, `data`) VALUES
(1, 'ja', 'fdgdfgfdgdfg', 'Janusz_Nosacz', '2019-01-08'),
(2, 'ja', 'fdgdfgfdgdfg', 'Janusz_Nosacz', '2019-01-08'),
(3, 'xos', 'dfsdfsd', 'user2', '2019-01-08'),
(5, 'hhh', 'ccccc', 'user2', '2019-01-08'),
(6, 'hhh', 'ccccc', 'Janusz_Nosacz', '2019-01-08'),
(7, 'aaa', 'bbbb', 'Janusz_Nosacz', '2019-01-08'),
(8, 'test', 'raport', 'Janusz_Nosacz', '2019-01-08'),
(9, 'TestowyRaportMVC', 'Testowy raport po wprowadzeniu wzorca MVC dla tworzenia raportów.', 'Janusz_Nosacz', '2019-01-14'),
(10, 'Wonsz rzeczny', 'dududud jest niebezpieczny', 'user2', '2019-01-22'),
(11, 'raport', '...sie na amen', 'user2', '2019-01-22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzet`
--

DROP TABLE IF EXISTS `sprzet`;
CREATE TABLE IF NOT EXISTS `sprzet` (
  `sprzet_id` int(11) NOT NULL AUTO_INCREMENT,
  `sprzet_kategoria_id` int(11) NOT NULL,
  `sprzet_opis` tinytext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `sprzet_czy_dostepny` tinyint(1) NOT NULL,
  PRIMARY KEY (`sprzet_id`),
  KEY `sprzet_kategoria_id` (`sprzet_kategoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `sprzet`
--

INSERT INTO `sprzet` (`sprzet_id`, `sprzet_kategoria_id`, `sprzet_opis`, `sprzet_czy_dostepny`) VALUES
(1, 2, 'Betoniarka 260l 1400W', 0),
(2, 2, 'Betoniarka 100l 850W', 0),
(3, 3, 'Drabina aluminiowa - 3 elementy x 6 szczebli', 0),
(4, 3, 'Drabina aluminiowa - 3 elementy x 10 szczebli', 0),
(5, 3, 'Drabina aluminiowa - 3 elementy x 14 szczebli', 1),
(6, 1, 'AGREGAT TRÓJFAZOWY HONDA: EP 15000\r\n\r\nTyp agregat trójfazowy 400V\r\nWaga: 210 kg\r\nMoc max.: 15,0kVA (400V) / 5,5kW (230V)\r\nMoc nominalna: 15,0kVA (400V) / 5,5kW (230V)\r\nGniazda AC: 2x230V 16A, 1x400V 16A\r\nLWA / Stopień ochrony: 94dB(A) / IP23', 0),
(24, 1, 'wiaderko', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzet_kategoria`
--

DROP TABLE IF EXISTS `sprzet_kategoria`;
CREATE TABLE IF NOT EXISTS `sprzet_kategoria` (
  `sprzet_kategoria_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategoria` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`sprzet_kategoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `sprzet_kategoria`
--

INSERT INTO `sprzet_kategoria` (`sprzet_kategoria_id`, `kategoria`) VALUES
(1, 'agregat'),
(2, 'betoniarka'),
(3, 'drabina');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `user_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `users_typ_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `users_typ_id` (`users_typ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci ROW_FORMAT=COMPACT;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `users_typ_id`) VALUES
(1, 'Janusz_Nosacz', 'passat19tdi', 2),
(2, 'PjoterNosacz', 'pjoterek69', 1),
(3, 'admin', 'admin', 3),
(4, 'user1', '1234', 1),
(5, 'user2', '1234', 2),
(6, 'user3', '1234', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users_typ`
--

DROP TABLE IF EXISTS `users_typ`;
CREATE TABLE IF NOT EXISTS `users_typ` (
  `users_typ_id` int(11) NOT NULL AUTO_INCREMENT,
  `typ` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`users_typ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users_typ`
--

INSERT INTO `users_typ` (`users_typ_id`, `typ`) VALUES
(1, 'inzynier_budowy'),
(2, 'kierownik_budowy'),
(3, 'operator_systemu');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zadanie`
--

DROP TABLE IF EXISTS `zadanie`;
CREATE TABLE IF NOT EXISTS `zadanie` (
  `zadanie_id` int(11) NOT NULL AUTO_INCREMENT,
  `zadanie_tytul` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `zadanie_tresc` tinytext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `lista_zadan_id` int(11) DEFAULT NULL,
  `Czy_Skonczone` tinyint(1) NOT NULL,
  PRIMARY KEY (`zadanie_id`),
  KEY `lista_zadan_id` (`lista_zadan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zadanie`
--

INSERT INTO `zadanie` (`zadanie_id`, `zadanie_tytul`, `zadanie_tresc`, `lista_zadan_id`, `Czy_Skonczone`) VALUES
(1, 'zbrojenia', 'zrobic zbroje pelnoplytowe - pan marian', 1, 1),
(2, 'trzewiki', 'zrobic trzewiki - pan andrzej', 1, 1),
(3, 'opancerzenie dla konia', 'pan Marian ma zrobic na wczoraj', 2, 1),
(4, 'Podkowy', 'Ktos ma wykonac podkowy koniec kropka', 1, 1),
(5, 'Umyc konia', 'kon m byc czysty i pachnacy najnowszym \r\nwisniowym\r\nmodlem\r\n\r\nrosmana\r\ninnego\r\nnie \r\nprzyjmuje\r\ndo\r\nwiadomosci\r\nkuce\r\nz\r\nbron', 1, 1),
(7, 'Hello world', 'Napisać hello world w HTML', 5, 1),
(9, 'Rakieta', 'Zbudować rakietę dla NASA', 20, 0),
(10, 'Podróż w czasie', 'Zbudować wehikuł czasu', 20, 0),
(19, 'Gra w szachy', 'Kot patuje Pata', 7, 0),
(32, 'sprzatanie', 'cos poszlo nie tak', 6, 0),
(33, 'skroty', 'robimy skrot przez park, przechodzimy obok monopolowego', 6, 0),
(34, 'Kot idzie na spacer', 'Wyprowadzić kota na spacer.', 8, 0),
(35, 'magazyn', 'edycja nie dziala(dziala jak zmniejszymy ilosc do 0)', 21, 1),
(36, 'ok', 'reszta na razie ok', 21, 0),
(37, '', '', 13, 0),
(38, '', '', 13, 0),
(48, 'Krok1', 'INCLUDE <conio.h>', 5, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zadanie_pracownicy`
--

DROP TABLE IF EXISTS `zadanie_pracownicy`;
CREATE TABLE IF NOT EXISTS `zadanie_pracownicy` (
  `zadanie_id` int(11) DEFAULT NULL,
  `pracownik_id` int(11) DEFAULT NULL,
  KEY `zadanie_id` (`zadanie_id`),
  KEY `pracownik_id` (`pracownik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zadanie_pracownicy`
--

INSERT INTO `zadanie_pracownicy` (`zadanie_id`, `pracownik_id`) VALUES
(9, 25),
(19, 25),
(19, 31),
(32, 31),
(33, 25),
(34, 25),
(34, 31),
(48, 35),
(48, 34);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia`
--

DROP TABLE IF EXISTS `zamowienia`;
CREATE TABLE IF NOT EXISTS `zamowienia` (
  `zamowienie_id` int(11) NOT NULL AUTO_INCREMENT,
  `zamowienie_opis` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `zamowienie_du` date NOT NULL,
  `zamowienie_status_id` int(11) NOT NULL,
  PRIMARY KEY (`zamowienie_id`),
  KEY `zamowienie_status_id` (`zamowienie_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zamowienia`
--

INSERT INTO `zamowienia` (`zamowienie_id`, `zamowienie_opis`, `zamowienie_du`, `zamowienie_status_id`) VALUES
(1, 'Zamówienie nr 69/2018\n\n-> 1000kg betonu\n-> 10 palet kostki brukowej firmy Bruk-Bet\n-> 1 paleta desek\n-> 4 solidne metalowe łopaty\n-> 12 par gumowych kaloszy', '2018-12-04', 4),
(2, 'Zamówienie nr 46/2018\n\n-> 250l farby bialej w opakowaniach po 25l\n-> 100m kabla energetycznego\n-> 50 rolek tasmy izolacyjnej', '2018-12-06', 2),
(5, 'Dodalem cos w pierwszej linijce\nDodalem teraz w drugiej!!!\n\nJest sobie jakies zamówienie.\n\nTutaj powinien byc opis.\n\nDluuuugi opis.\n\nAle nie za dlugi.\n\nCos tam jeszcze sobie dodam', '2019-01-12', 1),
(6, 'Zamówienie, którego nie da sie usunac, bo jest juz wysylane.', '2019-01-10', 3),
(8, 'Dodaje kolejne testowe zamówienie...', '2019-01-13', 1),
(9, 'pizza 60cm na cienkim, do tego sos pomidorowy, na wczoraj bo jestem glodny', '2019-01-22', 4),
(10, 'kebaba dla pracownikow + cola', '2019-01-22', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia_status`
--

DROP TABLE IF EXISTS `zamowienia_status`;
CREATE TABLE IF NOT EXISTS `zamowienia_status` (
  `zamowienie_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`zamowienie_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zamowienia_status`
--

INSERT INTO `zamowienia_status` (`zamowienie_status_id`, `status`) VALUES
(1, 'potwierdzone'),
(2, 'przygotowywane'),
(3, 'wysłane'),
(4, 'anulowane');

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `materialy`
--
ALTER TABLE `materialy`
  ADD CONSTRAINT `materialy_ibfk_1` FOREIGN KEY (`materialy_kategoria_id`) REFERENCES `materialy_kategoria` (`materialy_kategoria_id`);

--
-- Ograniczenia dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD CONSTRAINT `pracownicy_ibfk_1` FOREIGN KEY (`pracownik_status_id`) REFERENCES `pracownicy_status` (`pracownik_status_id`);

--
-- Ograniczenia dla tabeli `sprzet`
--
ALTER TABLE `sprzet`
  ADD CONSTRAINT `sprzet_ibfk_1` FOREIGN KEY (`sprzet_kategoria_id`) REFERENCES `sprzet_kategoria` (`sprzet_kategoria_id`);

--
-- Ograniczenia dla tabeli `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`users_typ_id`) REFERENCES `users_typ` (`users_typ_id`);

--
-- Ograniczenia dla tabeli `zadanie`
--
ALTER TABLE `zadanie`
  ADD CONSTRAINT `zadanie_ibfk_1` FOREIGN KEY (`lista_zadan_id`) REFERENCES `listy_zadan` (`lista_zadan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zadanie_pracownicy`
--
ALTER TABLE `zadanie_pracownicy`
  ADD CONSTRAINT `zadanie_pracownicy_ibfk_1` FOREIGN KEY (`zadanie_id`) REFERENCES `zadanie` (`zadanie_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `zadanie_pracownicy_ibfk_2` FOREIGN KEY (`pracownik_id`) REFERENCES `pracownicy` (`pracownik_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  ADD CONSTRAINT `zamowienia_ibfk_1` FOREIGN KEY (`zamowienie_status_id`) REFERENCES `zamowienia_status` (`zamowienie_status_id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
